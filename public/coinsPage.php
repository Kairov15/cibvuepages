<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<?
$hostName = $_SERVER['HTTP_HOST'];
$_SERVER["HTTPS"] ? $protocol = "https://" : $protocol = "http://";
$protocol = "https://";
$imagesDir = '/ru/services/cibVuePages/src/prodaja-monet/images/';
$jsonDir = '/ru/services/cibVuePages/src/prodaja-monet/coins.data.json';
$host = "{$protocol}{$hostName}";
$imagesPath = "{$protocol}{$hostName}{$imagesDir}";
$jsonPath = "{$protocol}{$hostName}{$jsonDir}";
?>
<head>
</head>
<noscript>
    <strong>We're sorry but creditbiz doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
</noscript>
    <script>
        window.region = '<?=$_SESSION["region"]?>'
        window.imagesPath= '<?=$imagesPath?>'
        window.jsonPath= '<?=$jsonPath?>'
    </script>
<div id="app"></div>

<body>
 
</body>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>