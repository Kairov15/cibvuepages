import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'
import Instructions from '../views/equiring-pages/instructions.vue'
import Equipment from '../views/equiring-pages/equipment.vue'
import Advantages from '../views/equiring-pages/advantages.vue'
import Connection from '../views/equiring-pages/connection.vue'
import Tariffs from '../views/equiring-pages/tariffs.vue'
const appPath = process.env.NODE_ENV === 'production' ? '/ru/biz/equiring' : '/equiring'
Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
            bcLinkText: 'Торговый эквайринг: договор торгового эквайринга, тарифы'
        },
        children: [
            {
                path: 'instructions',
                name: 'instructions',
                component: Instructions
            },
            {
                path: 'equipment',
                name: 'equipment',
                component: Equipment
            },
            {
                path: 'advantages',
                name: 'advantages',
                component: Advantages
            },
            {
                path: 'connection',
                name: 'connection',
                component: Connection
            },
            {
                path: 'tariffs',
                name: 'tariffs',
                component: Tariffs
            }
        ]
    }

]

const router = new VueRouter({
    mode: 'history',
    base: appPath,
    routes,
    store
})

const initFirstRoutePath = () => {
    const windowPathArr = window.location.pathname.split('/')
    const initialRoutePath = windowPathArr[windowPathArr.length - 1]
    if (windowPathArr.length > 2 && initialRoutePath) {
        router.replace({ name: initialRoutePath })
    } else {
        router.replace({ name: 'advantages' })
    }
}
initFirstRoutePath()

export default router
