export default [
    {
        title: 'Руководство',
        text: 'О ПОРЯДКЕ ПРЕДОСТАВЛЕНИЯ ОБСЛУЖИВАНИЯ ДЕРЖАТЕЛЯМ ПЛАТЁЖНЫХ КАРТ МЕЖДУНАРОДНЫХ ПЛАТЕЖНЫХ СИСТЕМ',
        img: require('@/equiring/assets/images/equiring-card/leadership.png'),
        pdfs: [
            'https://www.centrinvest.ru/files/equiring-instructions/leadership.pdf'
        ]
    },
    {
        title: 'Ingenico iCT 220/250',
        text: null,
        img: require('@/equiring/assets/images/equiring-card/Ingenico-iCT-1.png'),
        pdfs: [
            'https://www.centrinvest.ru/files/equiring-instructions/Ingenico_IWL2xx_ICT2xx(CISBASE).pdf',
            'https://www.centrinvest.ru/files/equiring-instructions/Ingenico_IWL2xx_ICT2xx(NEWWAY).pdf'
        ],
        modalImages: [
            require('@/equiring/assets/images/equiring-card/ingcb1.jpg'),
            require('@/equiring/assets/images/equiring-card/ingnw1.jpg')
        ]
    },
    {
        title: 'Ingenico iWL 220/250',
        text: null,
        img: require('@/equiring/assets/images/equiring-card/Ingenico-iWL-2.png'),
        pdfs: [
            'https://www.centrinvest.ru/files/equiring-instructions/Ingenico_IWL2xx_ICT2xx(CISBASE).pdf',
            'https://www.centrinvest.ru/files/equiring-instructions/Ingenico_IWL2xx_ICT2xx(NEWWAY).pdf'
        ],
        modalImages: [
            require('@/equiring/assets/images/equiring-card/ingcb1.jpg'),
            require('@/equiring/assets/images/equiring-card/ingnw1.jpg')
        ]
    },
    {
        title: 'Ingenico iPP320',
        text: null,
        img: require('@/equiring/assets/images/equiring-card/Ingenico-iPP-3.png'),
        pdfs: [

        ]
    },
    {
        title: 'Verifone VX520',
        text: null,
        img: require('@/equiring/assets/images/equiring-card/Verifone-VX520.png'),
        pdfs: [
            'https://www.centrinvest.ru/files/equiring-instructions/VeriFone_VX520(CISBASE).pdf',
            'https://www.centrinvest.ru/files/equiring-instructions/VeriFone_VX520(UNIPOS).pdf'
        ]
    },
    {
        title: 'Verifone VX675',
        text: null,
        img: require('@/equiring/assets/images/equiring-card/Verifone-VX675.png'),
        pdfs: [
            'https://www.centrinvest.ru/files/equiring-instructions/VeriFone_VX675_VX680(CISBASE).pdf',
            'https://www.centrinvest.ru/files/equiring-instructions/VeriFone_VX675_VX680(UNIPOS).pdf'
        ]
    },
    {
        title: 'Verifone VX680',
        text: null,
        img: require('@/equiring/assets/images/equiring-card/Verifone-VX680.png'),
        pdfs: [
            'https://www.centrinvest.ru/files/equiring-instructions/VeriFone_VX675_VX680(CISBASE).pdf',
            'https://www.centrinvest.ru/files/equiring-instructions/VeriFone_VX675_VX680(UNIPOS).pdf'
        ]
    },
    {
        title: 'Verifone VX820',
        text: null,
        img: require('@/equiring/assets/images/equiring-card/Verifone-VX820.png'),
        pdfs: [

        ]
    },
    {
        title: 'PAX D210',
        text: null,
        img: require('@/equiring/assets/images/equiring-card/PAX-D210-7.png'),
        pdfs: [
            'https://www.centrinvest.ru/files/equiring-instructions/PAX_D210.pdf'
        ]
    },
    {
        title: 'PAX S920',
        text: null,
        img: require('@/equiring/assets/images/equiring-card/PAX-S920.png'),
        pdfs: [
            'https://www.centrinvest.ru/files/equiring-instructions/PAX_S920.pdf'
        ]
    },
    {
        title: 'PAX A930',
        text: null,
        img: require('@/equiring/assets/images/equiring-card/PAX-A930-9.png'),
        pdfs: [
            'https://www.centrinvest.ru/files/equiring-instructions/PAX_A930.pdf'
        ]
    },
    {
        title: 'PAX 930+D190',
        text: null,
        img: require('@/equiring/assets/images/equiring-card/PAX-A930-D190.jpg'),
        pdfs: [
            'https://www.centrinvest.ru/files/equiring-instructions/PAX_A930_D190.pdf'
        ]
    },
    {
        title: 'Акт возврата',
        text: null,
        img: require('@/equiring/assets/images/equiring-card/leadership.png'),
        pdfs: [
            'https://www.centrinvest.ru/files/equiring-instructions/act-return.pdf'
        ]
    }
]
