import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '@/calendar/store'

const appPath = process.env.NODE_ENV === 'production' ? '/ru/calendar' : '/calendar'
Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        beforeEnter: (to, from, next) => {
            store.commit('setLoader', true)
            next()
        }
    },
    {
        path: '/month/:slug',
        name: 'Month',
        component: () => import('../views/month'),
        beforeEnter: (to, from, next) => {
            store.commit('setLoader', true)
            next()
        }
    },
    {
        path: '/month/:slug/:day',
        name: 'Day',
        component: () => import('../views/day')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: appPath,
    routes
})

export default router
