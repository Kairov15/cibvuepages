import Vue from 'vue'
import Vuex from 'vuex'
import {
    getField,
    updateField
} from 'vuex-map-fields'
import cyrillicToTranslit from 'cyrillic-to-translit-js'
import axios from 'axios'
import moment from 'moment'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        isDataLoad: false,
        year: 2021,
        data: [],
        showPreloader: true,
        isProduction: process.env.NODE_ENV === 'production',
        monthSlug: [
            'january',
            'february',
            'march',
            'april',
            'may',
            'june',
            'july',
            'august',
            'september',
            'october',
            'november',
            'december'
        ],
        monthName: [
            'Январь',
            'Февраль',
            'Март',
            'Апрель',
            'Май',
            'Июнь',
            'Июль',
            'Август',
            'Сентябрь',
            'Октябрь',
            'Ноябрь',
            'Декабрь'
        ]
    },
    getters: {
        getField,
        modData (state) {
            const copy = [...state.data]
            return copy.map(e => {
                return e.reduce((value, item) => {
                    let newItem = {}
                    const inArray = value.findIndex(element => {
                        return element.day === item.day
                    })
                    if (inArray !== -1) {
                        value[inArray].list.push({
                            label: item.label,
                            description: item.hasOwnProperty('description') ? item.description : '',
                            images: item.hasOwnProperty('images') ? item.images : []
                        })
                        return value
                    } else {
                        newItem = {
                            day: item.day,
                            list: [{
                                label: item.label,
                                description: item.hasOwnProperty('description') ? item.description : '',
                                images: item.hasOwnProperty('images') ? item.images : []
                            }]
                        }
                        return [...value, newItem]
                    }
                }, [])
            })
        },
        getDayIndex: (state, getters) => params => {
            if (params.index === -1) {
                return
            }
            return getters.modData[params.index].findIndex(item => {
                return item.day === params.day
            })
        },
        currentYear () {
            return moment().format('YYYY')
        },
        getAttrs: state => index => {
            return [
                ...state.data[index].map(item => ({
                    dates: new Date(2021, index, item.day),
                    highlight: item.color || 'blue',
                    customData: {
                        title: item.label,
                        class: 'someClass',
                        daySlug: item.daySlug,
                        day: item.day
                    },
                    popover: {
                        label: item.label,
                        placement: 'top'
                    }
                }))
            ]
        },
        getSlugIndex: state => slug => {
            return state.monthSlug.findIndex(item => {
                return item === slug
            })
        },
        getDay: (state, getters) => params => {
            if (params.index === -1) {
                return
            }
            return getters.modData[params.index].find(item => {
                return item.day === params.day
            })
        }
        // getSameDays: state => params => {
        //     return state.data[params.index].filter(item => {
        //         return item.day === params.day
        //     })
        // },

    },
    mutations: {
        updateField,
        setData (state, data) {
            const modifiedData = data.map(arr => {
                return arr.map(item => {
                    return {
                        ...item,
                        daySlug: cyrillicToTranslit().transform(item.label.toLowerCase(), '-')
                        }
                    })
                })
            state.data = modifiedData
        },
        setLoader (state, value) {
            state.showPreloader = value
        }
    },
    actions: {
        async getData ({ state, commit }) {
            const res = await axios.get('/calendarData.json')
            commit('setData', res.data)
            state.isDataLoad = true
            state.showPreloader = false
        }
    }
})
