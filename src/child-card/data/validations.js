import { required, sameAs } from 'vuelidate/lib/validators'
import ruDashSpace from '@/validators/ruDashSpace'
import phone from '@/validators/phone'
import emailFullCheck from '@/validators/emailFullCheck'

export default function validations () {
    return {
        formData: {
            fio: {
                required,
                ruDashSpace
            },
            phone: {
                required,
                phone
            },
            birthday: {
                required
            },
            nationality: {
                required
            },
            email: {
                required,
                emailFullCheck
            },
            agreement: {
                required,
                sameAs: sameAs(() => true)
            },
            pageUrl: {
                required
            }
        }
    }
}
