import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-sugar.css'
import VueMeta from 'vue-meta'
import { errorHandler } from '@/debet-karty/helpers'

Vue.prototype.$errorHandler = errorHandler
Vue.use(VueMeta)
Vue.use(VueToast)
Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
