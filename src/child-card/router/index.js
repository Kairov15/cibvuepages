import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

const appPath = process.env.NODE_ENV === 'production' ? '/ru/fiz/detskaya-karta' : '/child-card'
Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: () => import('../views/Home'),
    },
]

const router = new VueRouter({
    mode: 'history',
    base: appPath,
    routes,
    store
})

export default router
