import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'
const appPath = process.env.NODE_ENV === 'production' ? '/ru/fiz/potreb-kredit/' : '/potreb-kredit'
Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
            bcLinkText: 'Потребительский кредит'
        }
    }
]

const router = new VueRouter({
    mode: 'history',
    base: appPath,
    routes
})

export default router
