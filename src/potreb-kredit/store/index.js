import Vue from 'vue'
import Vuex from 'vuex'
import {
    getField,
    updateField
} from 'vuex-map-fields'
import cyrillicToTranslit from 'cyrillic-to-translit-js'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
    },
    getters: {
        getField

    },
    mutations: {
        updateField
    },
    actions: {},
    modules: {}
})
