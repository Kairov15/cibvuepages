import Vue from 'vue'
import Router from 'vue-router'
import CreditBiz from '../views/CreditBiz.vue'
import CreditBizInner from '../views/CreditBizInner.vue'
import Page404 from '../views/Page404'
import Meta from 'vue-meta'
import store from '../store/index'
const appPath = process.env.NODE_ENV === 'production' ? '/ru/biz/programmy-kreditovanija/' : '/kreditovaniye-biznesa'

Vue.use(Router)
Vue.use(Meta)
const router = new Router({
    base: appPath,
    mode: 'history',
    routes: [{
            path: '/',
            name: 'Программы кредитования ЮЛ',
            component: CreditBiz,
            meta: {
                bcLinkText: 'Кредитование бизнеса'
            }

        },
        {
            path: '/:slug',
            name: 'Внутренняя страница кредитования ЮЛ',
            component: CreditBizInner,
            meta: {
                dynamicPath: true,
                getter: 'oneProgramm',
                bcLinkText: (name) => name
            },
            beforeEnter: (to, from, next) => {
                store.dispatch('getOneProgramm', to.params.slug)
                    .then(() => {
                        next()
                        setTimeout(() => {
                            store.commit('togglePreloader', false)
                        }, 300)
                    })
            }
        },
        {
            path: '/404',
            name: '404',
            component: Page404
        },
        {
            path: '*',
            redirect: '/404'
        }
    ]
})

router.afterEach((to, from) => {
    const currentPath = window.location.pathname
    const regLinks = document.getElementsByClassName('regions__link')
    if (regLinks.length) {
        for (let i = 0; i < regLinks.length; i++) {
            const reg = regLinks[i].getAttribute('href').split('?')[1]
            regLinks[i].setAttribute('href', `${appPath}${to.fullPath}`.replace('//', '/').split('?')[0] + '?' + reg)
        }
    }
    if (from.name !== null && from.name !== to.name) {
        const path = `${currentPath}${to.fullPath}`.replace('//', '/')
        try {
            // eslint-disable-next-line no-undef
            dataLayer.push({
                event: 'viewVuePage',
                eventAction: 'Просмотр vue страниц',
                eventLabel: path
            })
        } catch (e) {
            console.log(e)
        }
    }
})

export default router
