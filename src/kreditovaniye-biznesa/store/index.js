import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from '../router'
Vue.use(Vuex)

export default new Vuex.Store({
    namespaced: true,
    strict: process.env.NODE_ENV !== 'production',
    state: {
        programms: {
            dataReceived: false,
            list: [],
            filteredList: []
        },
        oneProgramm: {
            data: {},
            dataReceived: false
        },
        filters: {
            dataReceived: false,
            currentFilter: 0,
            list: [{
                id: 0,
                title: 'Все программы'
            }]
        },
        docs: {
            list: {},
            dataReceived: false
        },
        programmTableSort: {
            rate_details: 'Ставка',
            amount_details: 'Сумма',
            prepay_details: 'Аванс',
            term_details: 'Срок',
            target: 'Цель',
            borrower: 'Заемщик',
            repay_details: 'Погашение',
            loan_security: 'Обеспечение'
        },
        showPreloader: true
    },
    getters: {
        allProgramms (state) {
            return state.programms
        },
        filters (state) {
            return state.filters
        },
        oneProgramm (state) {
            return state.oneProgramm
        },
        docsReceived (state) {
            return state.docs.dataReceived
        },
        allDocs (state) {
            return state.docs
        },
        showPreloader (state) {
            return state.showPreloader
        }
    },
    mutations: {
        setData (state, payload) {
            if (Array.isArray(payload.data)) {
                state[payload.state].list = state[payload.state].list.concat(payload.data)
            } else {
                state[payload.state].list = payload.data
            }
            state[payload.state].dataReceived = true
        },
        filterProgramms (state, id) {
            state.filters.currentFilter = id
            const progs = { ...state.programms }
            progs.list = progs.list.filter((item) => {
                if (id === 0) {
                    return true
                } else {
                    return item.filters.includes(id)
                }
            })
            state.programms.filteredList = progs.list
        },
        setOneProgramm (state, payload) {
            const programm = payload
            const programmTable = []
            for (const key in state.programmTableSort) {
                if (programm.data[key]) {
                    programmTable.push({
                        name: key,
                        label: state.programmTableSort[key],
                        value: programm.data[key]
                    })
                }
            }
            programm.data.programmTable = programmTable
            state.oneProgramm = programm
        },
        togglePreloader (state, payload) {
            state.showPreloader = payload
        }
    },
    actions: {
        getAllProgramms ({
            commit
        }) {
            commit('togglePreloader', true)
            axios.get('https://site-api.centrinvest.ru/api/v1/business/loan/programs')
                .then(res => {
                    commit('setData', {
                        data: res.data.payload.items,
                        state: 'programms'
                    })
                    commit('filterProgramms', 0)
                })
                .catch((error) => {
                    console.log(error)
                })
                .finally(() => {
                    commit('togglePreloader', false)
                })
        },
        getAllFilters ({
            commit
        }) {
            commit('togglePreloader', true)
            axios.get('https://site-api.centrinvest.ru/api/v1/business/loan/filters')
                .then(res => {
                    commit('setData', {
                        data: res.data.payload.items,
                        state: 'filters'
                    })
                })
                .catch((error) => {
                    console.log(error)
                })
                .finally(() => {
                    commit('togglePreloader', false)
                })
        },
        getOneProgramm ({
            commit
        }, slug) {
            return new Promise((resolve, reject) => {
                commit('togglePreloader', true)
                axios.get(`https://site-api.centrinvest.ru/api/v1/business/loan/programs/${slug}`)
                    .then(res => {
                        if (!res.data.error) {
                            commit('setOneProgramm', {
                                data: res.data.payload,
                                dataReceived: true
                            })
                        } else {
                            throw new Error(res.data.error)
                        }
                        resolve()
                    })
                    .catch((error) => {
                        console.log(error)
                        commit('setOneProgramm', {
                            data: {},
                            dataReceived: true
                        })
                        router.push({
                            name: 'Программы кредитования ЮЛ'
                        })
                    })
                    .finally(() => {
                        commit('togglePreloader', false)
                    })
            })
        },
        getDocs ({
            commit
        }) {
            commit('togglePreloader', true)
            axios.get('https://site-api.centrinvest.ru/api/v1/business/loan/common-documents')
                .then((res) => {
                    commit('setData', {
                        data: res.data.payload,
                        state: 'docs'
                    })
                })
                .catch((error) => {
                    console.log(error)
                })
                .finally(() => {
                    commit('togglePreloader', false)
                })
        },
        filterProgramms ({
            commit
        }, id) {
            commit('filterProgramms', id)
        },
        getData ({
            dispatch,
            getters
        }) {
            if (!getters.filters.dataReceived) {
                dispatch('getAllFilters')
            }
            if (!getters.allProgramms.dataReceived) {
                dispatch('getAllProgramms')
            }
            if (!getters.allDocs.dataReceived) {
                dispatch('getDocs')
            }
        }
    }
})
