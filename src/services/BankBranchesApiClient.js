import axios from 'axios'
const debug = process.env.NODE_ENV !== 'production' || window.location.host !== 'www.centrinvest.ru'
const YMAP_API_COORDINATE_URL = `${debug ? 'https://showroom25.dev.cinet.ru/' : '/'}cabinet/ajax/yandexmapcoordinate2/`
const SERVICES_OPERU = 'requestCards'

const BankBranchesApiClient = {
    async getFullRegions (service) {
        try {
            const response = await axios.get(YMAP_API_COORDINATE_URL, { params: { call: 'getFullRegions', dataQuery: service } })
            return response.data
        } catch (e) {
            return e
        }
    },
    async getFullOffices (service) {
        try {
            const response = await axios.get(YMAP_API_COORDINATE_URL, { params: { call: 'getFullOffices', dataQuery: service } })
            return response.data
        } catch (e) {
            return e
        }
    },
    async getMapPoints (service) {
        try {
            const response = await axios.get(YMAP_API_COORDINATE_URL, { params: { call: 'getPoints', dataQuery: service } })
            return response.data
        } catch (e) {
            return e
        }
    }
}

export { BankBranchesApiClient, SERVICES_OPERU }
