<?php

PHP_SAPI === 'cli' or die('not allowed');

require_once __DIR__ . "/config.php";

function logger($message, $skipDate = false) {
    if ($skipDate) {
        echo "{$message}";
    } else {
        $dt = date('d.m.Y H:i:s');
        echo "[{$dt}] - {$message}";
    }
}

function logger_n($message, $skipDate = false) {
    logger($message . PHP_EOL, $skipDate);
}

if (!isset($config)) {
    logger_n("ERROR: Configuration file is not found");
    exit();
}

try {
    $url = sprintf("%s%s", $config['api']['host'], $config['api']['url']);
    logger("Request to {$url}...");
    $options = [
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_HEADER          => false,
        CURLOPT_FOLLOWLOCATION  => true,
        CURLOPT_ENCODING        => "",
        CURLOPT_AUTOREFERER     => true,
        CURLOPT_CONNECTTIMEOUT  => 120,
        CURLOPT_TIMEOUT         => 120,
        CURLOPT_MAXREDIRS       => 10,
        CURLOPT_SSL_VERIFYPEER  => false
    ];

    $ch = curl_init($url);
    curl_setopt_array($ch, $options);

    $content = curl_exec($ch);
    $error = curl_errno($ch);
    $message = curl_errno($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);

    $data = json_decode($content, true);
    logger_n(" done!", true);
    if ($data['status'] === 0 && is_array($data['payload']) && count($data['payload']) > 0) {
        logger_n("Data saved!");
        file_put_contents(__DIR__ . "/" . $config['storage'], json_encode($data['payload']));
        foreach ($data['payload'] as $item) {
            if (array_key_exists('coins', $item)) {
                foreach($item['coins'] as $coin) {
                    if (array_key_exists('image', $coin) && array_key_exists('imageSrc', $coin)) {
                        logger_n("Check image {$coin['image']}:");
                        $filename = __DIR__ . "/" . $config['images_folder'] . "/" . $coin['image'];
                        if (!file_exists($filename)) {
                            logger("Download image {$coin['imageSrc']}...");
                            $imageData = file_get_contents(sprintf("%s/%s", $config['api']['host'], $coin['imageSrc']));
                            file_put_contents($filename, $imageData);
                            logger_n(" done!", true);
                        } else {
                            logger_n("Image {$coin['image']} already exists.");
                        }
                    }
                }
            }
        }
    } else {
        logger_n("ERROR: Empty response");
    }
} catch (\Exception $e) {
    logger_n("ERROR: " . $e->getMessage());
    exit();
}