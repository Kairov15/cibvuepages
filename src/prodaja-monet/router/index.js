import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'
const appPath = process.env.NODE_ENV === 'production' ? '/ru/fiz/prodaja-monet/' : '/prodaja-monet'
Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
            bcLinkText: 'Продажа монет из драгоценных металлов'
        }
    }
]

const router = new VueRouter({
    mode: 'history',
    base: appPath,
    routes
})

export default router
