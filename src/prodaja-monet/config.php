<?php

$config = [
    'api' => [
        'host'  => "https://site-api.centrinvest.ru",
        'url'   => "/api/plugin/coins/v1/category-list"
    ],
    'storage'       => "coins.data.json",
    'images_folder' => "images"
];

if (file_exists(__DIR__ . "/config.dev.php")) {
    include_once __DIR__ . "/config.dev.php";
}