import Vue from 'vue'
import Vuex from 'vuex'
import {
    getField,
    updateField
} from 'vuex-map-fields'
import cyrillicToTranslit from 'cyrillic-to-translit-js'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        cardType: 'universal',
        pageTitle: '',
        onePage: {
            data: {
                title: 'Универсальная карта мир'
            },
            dataReceived: false
        }
    },
    getters: {
        getField,
        onePage (state) {
            return state.onePage
        }

    },
    mutations: {
        updateField,
        setTitle (state, title) {
            const translit = new cyrillicToTranslit()
            const result = translit.reverse(title).replace(/_/g, ' ')
            state.pageTitle = result
        }
    },
    actions: {},
    modules: {}
})
