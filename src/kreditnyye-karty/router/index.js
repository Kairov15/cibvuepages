import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'
const appPath = process.env.NODE_ENV === 'production' ? '/ru/fiz/kreditovanie/kreditnye-karty/discount-period' : '/kreditnyye-karty'
Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
            bcLinkText: 'Банковская карта с льготным периодом'
        }
    },
    {
        path: '/:slug',
        name: 'innerPage',
        meta: {
            getter: 'onePage',
            bcLinkText: (name) => name
        },
        component: () => import('../views/innerPage.vue'),
        beforeEnter: (to, from, next) => {
            store.commit('setTitle', to.params.slug)
            next()
        }
    }
]

const router = new VueRouter({
    mode: 'history',
    base: appPath,
    routes
})

export default router
