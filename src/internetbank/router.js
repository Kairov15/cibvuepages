import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import PaymentsDocList from './views/PaymentsDocList.vue'
import CurrencyOperationsList from './views/CurrencyOperationsList.vue'
import Login from './views/Login.vue'
import Cabinet from './views/Cabinet.vue'
import Settings from './views/cabinet/Settings'
import CabinetContent from './views/cabinet/CabinetContent'
import Payments from './views/cabinet/Payments'
import Score from './views/cabinet/Score'
import Charge from './views/cabinet/Charge'
import ChargeRub from './views/cabinet/ChargeRub'
import Currency from './views/cabinet/Currency'
import newCurrency from './views/cabinet/newCurrency'
const appPath = process.env.NODE_ENV === 'production' ? '/ru/internetbank/' : '/internetbank'
Vue.use(Router)

export default new Router({
    base: appPath,
    routes: [{
            path: '/',
            name: 'Home',
            component: Home,
            meta: {
                label: 'Главная'
            }
        },
        {
            path: '/PaymentsDocList',
            name: 'PaymentsDocList',
            component: PaymentsDocList,
            meta: {
                label: 'Платежные документы'
            }
        },
        {
            path: '/CurrencyOperationsList',
            name: 'CurrencyOperationsList',
            component: CurrencyOperationsList,
            meta: {
                label: 'Валютные операции'
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                label: 'Общий обзор'
            },
            props: {
                scenarioType: 'generalReview'
            }
        },

        {
            path: '/:scenarioType/cabinet',
            component: Cabinet,
            meta: {
                label: 'Кабинет'
            },
            props: true,
            children: [
                {
                    path: '',
                    component: CabinetContent,
                    name: 'CabinetContent'
                },
                {
                    path: 'settings',
                    component: Settings,
                    name: 'settings'
                },
                {
                    path: 'payments',
                    component: Payments,
                    name: 'payments'
                },
                {
                    path: 'charge',
                    component: Charge,
                    name: 'charge'
                },
                {
                    path: 'chargerub',
                    component: ChargeRub,
                    name: 'chargerub'
                },
                {
                    path: 'currency',
                    component: Currency,
                    name: 'currency'
                },
                {
                    path: 'newCurrency',
                    component: newCurrency,
                    name: 'newCurrency'
                },
                {
                    path: 'score',
                    component: Score,
                    name: 'score'
                }
            ]
        }
    ]
})
