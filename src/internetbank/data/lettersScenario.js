export default {
    name: 'Письма',
        currentStep: 0,
    scenario: [{
    actionTypeCallBack: 'openMenu',
    elementId: null,
    componentId: null,
    hintText: 'Входящая и исходящая корреспонденция',
    menuLabel: 'Письма',
    undefinedItem: true
},
    {
        actionTypeCallBack: null,
        elementId: null,
        componentId: null,
        hintText: false,
        menuLabel: 'в банк',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'closeMenu',
        elementId: null,
        componentId: null,
        hintText: false,
        menuLabel: 'Из банка',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openMenu',
        elementId: null,
        componentId: null,
        hintText: 'Обработка и представление в наглядном виде информации о входящих и исходящих платежах, планирование платежей по определенным счетам, создание напоминаний, а также отображение информации о всех операциях в системе по датам',
        menuLabel: 'Аналитика',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openMenu',
        elementId: null,
        componentId: null,
        hintText: 'Создание, редактирование и удаление напоминаний о запланированных операциях в системе, а также отображение информации о данных напоминаниях по датам',
        menuLabel: 'Календарь',
        undefinedItem: true
    },
    {
        actionTypeCallBack: null,
        elementId: null,
        componentId: null,
        hintText: 'Создание разовых или регулярных напоминаний',
        menuLabel: 'Напоминания',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'closeMenu',
        elementId: null,
        componentId: null,
        hintText: 'Просмотр всех операций',
        menuLabel: 'История операций',
        undefinedItem: true
    },
    {
        actionTypeCallBack: null,
        elementId: null,
        componentId: null,
        hintText: 'Входящие и исходящие платежи на графике за определенный период',
        menuLabel: 'График оборотов',
        undefinedItem: true
    },
    {
        actionTypeCallBack: null,
        elementId: null,
        componentId: null,
        hintText: 'Просмотр всех входящих и исходящих платежей с возможностью сортировки по дате, контрагенту, сумме и т.д.',
        menuLabel: 'Фильтр платежей',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'closeMenu',
        elementId: null,
        componentId: null,
        hintText: 'Формирование графика платежей по выбранным параметрам. При формировании планового платежа возможно создать напоминание о платеже. В дальнейшем система будет выводить уведомление о необходимости выполнения платежа',
        menuLabel: 'Плановый платеж',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openMenu',
        elementId: null,
        componentId: null,
        hintText: 'Меню быстрого перехода к корреспонденции',
        menuLabel: 'Файл',
        undefinedItem: true
    },
    {
        actionTypeCallBack: null,
        elementId: null,
        componentId: null,
        hintText: false,
        menuLabel: 'Входящие сообщения',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'closeMenu',
        elementId: null,
        componentId: null,
        hintText: false,
        menuLabel: 'Исходящие сообщения',
        undefinedItem: true
    }
]
}
