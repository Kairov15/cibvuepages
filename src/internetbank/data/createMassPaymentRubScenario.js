export default {
    name: 'Рублевые платежи, импорт из 1С',
        currentStep: 0,
    scenario: [
    {
        actionTypeCallBack: 'openMenu',
        elementId: null,
        componentId: null,
        hintText: 'Создание, подписание, импорт из 1С платежных поручений',
        menuLabel: 'Платежные документы',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openSettings',
        actionTypeBackCallBack: 'closeMenu("Платежные документы")',
        elementId: null,
        componentId: null,
        hintText: 'Создание платежного поручения, импорт платежного поручения',
        menuLabel: 'Рублевые платежи',
        redirectPath: '/createMassPaymentRub/cabinet/chargeRub',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openImport',
        actionTypeBackCallBack: 'openMenu("Платежные документы");redirect("/createMassPaymentRub/cabinet")',
        elementId: 'importCreated',
        componentId: 'importCreated',
        hintText: 'Импорт платежных поручений из 1С или в XML формате',
        menuLabel: 'Импорт',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openModal',
        actionTypeBackCallBack: 'closeImport()',
        elementId: 'importItem',
        componentId: 'importItem',
        hintText: 'Импорт в формате 1С: импортировать из 1С',
        menuLabel: 'Импорт в формате 1С',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'isActiveButton',
        actionTypeBackCallBack: 'openImport();closeModal()',
        data: '1c_to_kl',
        elementId: 'fileImport',
        hintText: 'Выберите файл с платежными поручениями, сформированный в программе 1С'
    },
    {
        actionTypeCallBack: 'isActiveFalse',
        elementId: 'fileTest',
        hintText: 'Нажмите импортировать'
    },
    {
        actionTypeCallBack: 'closeModal',
        elementId: 'modalClose',
        componentId: 'modalClose',
        menuLabel: 'Закрыть',
        hintText: 'Нажмите закрыть',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'writeLineRub',
        actionTypeBackCallBack: 'openModal()',
        elementId: 'line',
        componentId: 'line',
        menuLabel: 'test',
        undefinedItem: true,
        hintText: 'Выделите созданный рублевый платеж '

    },
    {
        actionTypeCallBack: null,
        elementId: 'writeCharge',
        componentId: 'writeCharge',
        hintText: 'Нажмите подписать',
        menuLabel: 'Подписать',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'falseWriteLine',
        elementId: 'toSend',
        componentId: 'toSend',
        hintText: 'Нажмите отправить',
        menuLabel: 'Отправить',
        undefinedItem: true
    }
]
}
