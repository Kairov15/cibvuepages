export default {
    currentStep: 0,
    scenario: [{
        actionTypeCallBack: 'fill',
        data: 'admin',
        elementId: 'name',
        hintText: 'Введите логин'
        },
        {
            actionTypeCallBack: 'fill',
            data: 'password',
            elementId: 'pass',
            hintText: 'Введите пароль'
        },
        {
            actionTypeCallBack: 'redirect',
            elementId: 'login',
            hintText: 'Вход в систему',
            redirectPath: '/generalReview/cabinet'
        },
        {
            actionTypeCallBack: null,
            elementId: 'userName',
            componentId: 'userName',
            hintText: 'ФИО пользователя авторизовавшегося в системе "Центр-инвест" Бизнес-онлайн"'
        },
        {
            actionTypeCallBack: null,
            elementId: 'menu',
            componentId: 'menu',
            hintText: 'Основное меню'
        },
        {
            actionTypeCallBack: null,
            elementId: 'score',
            componentId: 'score',
            hintText: 'Список счетов организации и актуальная информация по ним, с возможностью формирования выписки'
        },
        {
            actionTypeCallBack: null,
            elementId: 'payments',
            componentId: 'payments',
            hintText: 'Список последних исходящих и входящих платежей с указанием статуса платежного поручения и возможностью перейти к оформлению нового платежного поручения'
        },
        {
            actionTypeCallBack: null,
            elementId: 'news',
            componentId: 'news',
            hintText: 'Сообщения общеинформационного характера'
        },
        {
            actionTypeCallBack: null,
            elementId: 'services',
            componentId: 'services',
            hintText: 'Информация об услугах Электронного Офиса, подключенных и доступных для подключения через электронный офис, с указанием статуса подключения и возможностью перейти к оформлению (просмотру) заявки на подключение / отключение услуги'
        },
        {
            actionTypeCallBack: null,
            elementId: null,
            componentId: null,
            hintText: 'Меню быстрого перехода к корреспонденции, календарю и настройкам',
            menuLabel: 'contextMenu',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'openMenu',
            elementId: null,
            componentId: null,
            hintText: 'Меню быстрого перехода к корреспонденции',
            menuLabel: 'Файл',
            undefinedItem: true
        },
        {
            actionTypeCallBack: null,
            actionTypeBackCallBack: 'closeMenu("Входящие сообщения")',
            elementId: null,
            componentId: null,
            hintText: 'Входящая корреспонденция',
            menuLabel: 'Входящие сообщения',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'closeMenu',
            elementId: null,
            componentId: null,
            hintText: 'Исходящая корреспонденция',
            menuLabel: 'Исходящие сообщения',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'openMenu',
            actionTypeBackCallBack: 'openMenu("Файл")',
            elementId: null,
            componentId: null,
            hintText: 'Настройки системы: создание сертификата, смена пароля, справочники, шаблоны и т.д.',
            menuLabel: 'Настройки',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'openSettings',
            actionTypeBackCallBack: 'closeMenu("Настройки")',
            elementId: null,
            componentId: null,
            hintText: 'Меню настройки главной страницы',
            menuLabel: 'Настройки главной страницы',
            redirectPath: '/generalReview/cabinet/settings',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'setCheckBoxs',
            actionTypeBackCallBack: 'openMenu("Настройки");redirect("/generalReview/cabinet")',
            elementId: 'cabinet-widget-table',
            componentId: 'cabinet-widget-table',
            hintText: 'Выбор отображения и порядка расположения виджетов на главной странице'
        },
        {
            actionTypeCallBack: 'openMain',
            elementId: null,
            componentId: null,
            hintText: 'Переход на главную страницу',
            menuLabel: 'Главная',
            redirectPath: '/generalReview/cabinet',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'openMenu',
            actionTypeBackCallBack: 'redirect("/generalReview/cabinet/settings")',
            elementId: null,
            componentId: null,
            hintText: 'Информация по счетам, выписки и запросы на получение выписок',
            menuLabel: 'Счета',
            undefinedItem: true
        },
        {
            actionTypeCallBack: null,
            actionTypeBackCallBack: 'closeMenu("Счета")',
            elementId: null,
            componentId: null,
            hintText: 'Подробная информация о состоянии счетов',
            menuLabel: 'Информация по счетам',
            undefinedItem: true
        },
        {
            actionTypeCallBack: null,
            elementId: null,
            componentId: null,
            hintText: 'Переход в раздел «Выписки»',
            menuLabel: 'Выписки',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'closeMenu',
            elementId: null,
            componentId: null,
            hintText: 'Список запросов на получение выписок',
            menuLabel: 'Запросы на получение выписки',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'openMenu',
            actionTypeBackCallBack: 'openMenu("Счета")',
            elementId: null,
            componentId: null,
            hintText: 'Создание, подписание, импорт из 1С платежных поручений',
            menuLabel: 'Платежные документы',
            undefinedItem: true
        },
        {
            actionTypeCallBack: null,
            actionTypeBackCallBack: 'closeMenu("Платежные документы")',
            elementId: null,
            componentId: null,
            hintText: 'Создание платежного поручения, импорт платежного поручения',
            menuLabel: 'Рублевые платежи',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'closeMenu',
            elementId: null,
            componentId: null,
            hintText: 'Создание множества платежных поручений одновременно',
            menuLabel: 'Массовые платежи',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'openMenu',
            actionTypeBackCallBack: 'openMenu("Платежные документы")',
            elementId: null,
            componentId: null,
            hintText: 'Валютное РКО и Валютный контроль',
            menuLabel: 'Валютные операции',
            undefinedItem: true
        },
        {
            actionTypeCallBack: null,
            actionTypeBackCallBack: 'closeMenu("Валютные операции")',
            elementId: null,
            componentId: null,
            hintText: 'Список поручений на совершение валютных операций и уведомлений',
            menuLabel: 'Поручения',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'closeMenu',
            elementId: null,
            componentId: null,
            hintText: 'Документы и сведения , касающиеся валютного контроля и ВЭД',
            menuLabel: 'Валютный контроль',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'openMenu',
            actionTypeBackCallBack: 'openMenu("Валютные операции")',
            elementId: null,
            componentId: null,
            hintText: 'Входящая и исходящая корреспонденция',
            menuLabel: 'Письма',
            undefinedItem: true
        },
        {
            actionTypeCallBack: null,
            actionTypeBackCallBack: 'closeMenu("Письма")',
            elementId: null,
            componentId: null,
            hintText: 'Письма отправленные в банк',
            menuLabel: 'В банк',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'closeMenu',
            elementId: null,
            componentId: null,
            hintText: 'Входящая корреспонденция из банка',
            menuLabel: 'Из банка',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'openMenu',
            actionTypeBackCallBack: 'openMenu("Письма")',
            elementId: null,
            componentId: null,
            hintText: 'Обработка и представление в наглядном виде информации о входящих и исходящих платежах, планирование платежей по определенным счетам, создание напоминаний, а также отображение информации о всех операциях в системе по датам',
            menuLabel: 'Аналитика',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'openMenu',
            actionTypeBackCallBack: 'closeMenu("Аналитика")',
            elementId: null,
            componentId: null,
            hintText: 'Создание, редактирование и удаление напоминаний о запланированных операциях в системе, а также отображение информации о данных напоминаниях по датам',
            menuLabel: 'Календарь',
            undefinedItem: true
        },
        {
            actionTypeCallBack: null,
            elementId: null,
            componentId: null,
            hintText: 'Создание разовых или регулярных напоминаний',
            menuLabel: 'Напоминания',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'closeMenu',
            elementId: null,
            componentId: null,
            hintText: 'Просмотр всех операций',
            menuLabel: 'История операций',
            undefinedItem: true
        },
        {
            actionTypeCallBack: null,
            elementId: null,
            componentId: null,
            hintText: 'Входящие и исходящие платежи на графике за определенный период',
            menuLabel: 'График оборотов',
            undefinedItem: true
        },
        {
            actionTypeCallBack: null,
            elementId: null,
            componentId: null,
            hintText: 'Просмотр всех входящих и исходящих платежей с возможностью сортировки по дате, контрагенту, сумме и т.д.',
            menuLabel: 'Фильтр платежей',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'closeMenu',
            elementId: null,
            componentId: null,
            hintText: 'Формирование графика платежей по выбранным параметрам. При формировании планового платежа возможно создать напоминание о платеже. В дальнейшем система будет выводить уведомление о необходимости выполнения платежа',
            menuLabel: 'Плановый платеж',
            undefinedItem: true
        },
        {
            actionTypeCallBack: 'openMenu',
            actionTypeBackCallBack: 'openMenu("Аналитика")',
            elementId: null,
            componentId: null,
            hintText: 'Кредиты, депозиты, зарплатный проект, эквайринг',
            menuLabel: 'Продукты и услуги',
            undefinedItem: true
        },
        {
            actionTypeCallBack: null,
            actionTypeBackCallBack: 'closeMenu("Продукты и услуги")',
            elementId: 'products-and-services-subitem',
            componentId: 'products-and-services-subitem',
            hintText: 'Другие продукты и услуги'
        }
    ]
}
