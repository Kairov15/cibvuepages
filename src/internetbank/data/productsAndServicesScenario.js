export default {
    name: 'Продукты и услуги',
        currentStep: 0,
    scenario: [{
    actionTypeCallBack: 'openMenu',
    elementId: null,
    componentId: null,
    hintText: false,
    menuLabel: 'Продукты и услуги',
    undefinedItem: true
},
    {
        actionTypeCallBack: 'openMenu',
        elementId: null,
        componentId: null,
        hintText: false,
        menuLabel: 'Электронный офис',
        undefinedItem: true
    },
    {
        actionTypeCallBack: null,
        elementId: null,
        componentId: null,
        hintText: false,
        menuLabel: 'Подключение услуг',
        undefinedItem: true
    },
    {
        actionTypeCallBack: null,
        elementId: null,
        componentId: null,
        hintText: false,
        menuLabel: 'Отключение услуг',
        undefinedItem: true
    },
    {
        actionTypeCallBack: null,
        elementId: null,
        componentId: null,
        hintText: false,
        menuLabel: 'Запросы справок',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'closeMenu',
        elementId: null,
        componentId: null,
        hintText: false,
        menuLabel: 'Запросы на отзыв документов',
        undefinedItem: true
    }
]
}
