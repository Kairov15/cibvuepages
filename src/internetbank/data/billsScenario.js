export default {
    name: 'Счета',
        currentStep: 0,
    scenario: [
    {
        actionTypeCallBack: 'openMenu',
        elementId: null,
        componentId: null,
        hintText: 'Информация по счетам, выписки и запросы на получение выписок',
        menuLabel: 'Счета',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openSettings',
        elementId: null,
        componentId: null,
        hintText: 'Подробная информация о состоянии счетов',
        menuLabel: 'Информация по счетам',
        redirectPath: '/bills/cabinet/score',
        undefinedItem: true
    },
    {
        actionTypeCallBack: null,
        elementId: 'scoreExtracts',
        componentId: 'scoreExtracts',
        hintText: 'Формирование выписок'
    },
    {
        actionTypeCallBack: null,
        elementId: 'scoreStory',
        componentId: 'scoreStory',
        hintText: 'Просмотр истории запросов выписок'
    },
    {
        actionTypeCallBack: null,
        elementId: 'scoreInput1',
        componentId: 'scoreInput1',
        hintText: 'Быстрый поиск счета по номеру'
    },
    {
        actionTypeCallBack: null,
        elementId: 'scoreInput2',
        componentId: 'scoreInput2',
        hintText: 'Валюта счета'
    },
    {
        actionTypeCallBack: null,
        elementId: 'scoreInput3',
        componentId: 'scoreInput3',
        hintText: 'Тип счета'
    },
    {
        actionTypeCallBack: 'openMain',
        elementId: 'scoreView',
        componentId: 'scoreView',
        hintText: false,
        redirectPath: '/bills/cabinet'
    },
    {
        actionTypeCallBack: 'openMenu',
        elementId: null,
        componentId: null,
        hintText: false,
        menuLabel: 'Счета',
        undefinedItem: true
    },
    {
        actionTypeCallBack: null,
        elementId: null,
        componentId: null,
        hintText: 'Переход в раздел «Выписки»',
        menuLabel: 'Выписки',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'closeMenu',
        elementId: null,
        componentId: null,
        hintText: 'Список запросов на получение выписок',
        menuLabel: 'Запросы на получение выписки',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openMenu',
        elementId: null,
        componentId: null,
        hintText: 'Создание, подписание, импорт из 1С платежных поручений',
        menuLabel: 'Платежные документы',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openSettings',
        elementId: null,
        componentId: null,
        hintText: false,
        menuLabel: 'Рублевые платежи',
        redirectPath: '/bills/cabinet/payments',
        undefinedItem: true
    },
    {
        actionTypeCallBack: null,
        elementId: 'buttonCreated',
        componentId: 'buttonCreated',
        hintText: 'Создание платежного поручения'
    },
    {
        actionTypeCallBack: 'openMain',
        elementId: 'importCreated',
        componentId: 'importCreated',
        hintText: 'Импорт платежного поручения из 1С',
        redirectPath: '/bills/cabinet'
    },
    {
        actionTypeCallBack: 'openMenu',
        elementId: null,
        componentId: null,
        hintText: false,
        menuLabel: 'Платежные документы',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'closeMenu',
        elementId: null,
        componentId: null,
        hintText: 'Создание множества платежных поручений одновременно',
        menuLabel: 'Массовые платежи',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openMenu',
        elementId: null,
        componentId: null,
        hintText: 'Валютные операции',
        menuLabel: 'Валютные операции',
        undefinedItem: true
    },
    {
        actionTypeCallBack: null,
        elementId: null,
        componentId: null,
        hintText: false,
        menuLabel: 'Поручения',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'closeMenu',
        elementId: null,
        componentId: null,
        hintText: false,
        menuLabel: 'Валютный контроль',
        undefinedItem: true
    }
]
}
