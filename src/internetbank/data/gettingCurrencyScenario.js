export default {
    name: 'Получение валюты',
        currentStep: 0,
    scenario: [
    {
        actionTypeCallBack: 'openMenu',
        elementId: null,
        componentId: null,
        hintText: 'Валютное РКО и Валютный контроль',
        menuLabel: 'Валютные операции',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openMenu',
        actionTypeBackCallBack: 'closeMenu("Валютные операции")',
        elementId: null,
        componentId: null,
        hintText: 'Список поручений на совершение валютных операций и уведомлений',
        menuLabel: 'Поручения',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openSettings',
        actionTypeBackCallBack: 'closeSingleMenu("Поручения")',
        elementId: null,
        componentId: null,
        hintText: false,
        menuLabel: 'Распоряжения на списание средств с транзитного валютного счета',
        redirectPath: '/gettingCurrency/cabinet/currency',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openModal',
        actionTypeBackCallBack: 'redirect("/gettingCurrency/cabinet");openMenu("Валютные операции");openMenu("Поручения")',
        elementId: 'createCurrency',
        componentId: 'createCurrency',
        hintText: false,
        menuLabel: 'Создать',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'isActiveFalse',
        actionTypeBackCallBack: 'closeModal()',
        elementId: 'executor'
    },
    {
        actionTypeCallBack: 'openMiniModal',
        elementId: 'add',
        componentId: 'add',
        hintText: false,
        menuLabel: 'Добавить',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'isActiveFalse1',
        actionTypeBackCallBack: 'closeMiniModal()',
        elementId: 'number'
    },
    {
        actionTypeCallBack: 'isActiveFalse2',
        elementId: 'date'
    },
    {
        actionTypeCallBack: 'isActiveFalse3',
        elementId: 'sum'
    },
    {
        actionTypeCallBack: 'isActiveFalse4',
        elementId: 'thausend'
    },
    {
        actionTypeCallBack: 'isActiveFalse5',
        elementId: 'usd'
    },
    {
        actionTypeCallBack: 'closeMiniModal',
        elementId: 'save',
        componentId: 'save',
        hintText: false,
        menuLabel: 'Сохранить',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openSumTab',
        actionTypeBackCallBack: 'openMiniModal();openMainTab()',
        elementId: 'sumAnd',
        componentId: 'sumAnd',
        hintText: 'Перейти в раздел "Сумма и реквизиты"',
        menuLabel: 'Сумма и реквизиты',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'closeModal',
        elementId: 'writeOut',
        componentId: 'writeOut',
        hintText: false,
        menuLabel: 'Подписать и отправить',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openMenu',
        actionTypeBackCallBack: 'openModal()',
        elementId: null,
        componentId: null,
        hintText: 'Валютное РКО и Валютный контроль',
        menuLabel: 'Валютные операции',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openMenu',
        actionTypeBackCallBack: 'closeMenu("Валютные операции")',
        elementId: null,
        componentId: null,
        hintText: 'Основные документы валютного контроля',
        menuLabel: 'Валютный контроль',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openSettings',
        actionTypeBackCallBack: 'closeSingleMenu("Валютный контроль")',
        elementId: null,
        componentId: null,
        hintText: false,
        menuLabel: 'Информация об уникальном номере контраката и коде вида операции',
        redirectPath: '/gettingCurrency/cabinet/newCurrency',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openModal',
        actionTypeBackCallBack: 'redirect("/gettingCurrency/cabinet/currency");openMenu("Валютные операции");openMenu("Валютный контроль")',
        elementId: 'createCurrency',
        componentId: 'createCurrency',
        hintText: false,
        menuLabel: 'Создать',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openMiniModal',
        actionTypeBackCallBack: 'closeModal()',
        elementId: 'add',
        componentId: 'add',
        hintText: false,
        menuLabel: 'Добавить',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'openMiniCurrentModal',
        actionTypeBackCallBack: 'closeMiniModal()',
        elementId: 'modalmini',
        componentId: 'modalmini',
        hintText: false,
        menuLabel: 'Добавление записи',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'closeMiniModal',
        actionTypeBackCallBack: 'closeMiniCurrentModal()',
        elementId: 'saveCurrent',
        componentId: 'saveCurrent',
        hintText: false,
        menuLabel: 'Сохранить',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'closeMiniModal',
        actionTypeBackCallBack: 'openMiniModal()',
        elementId: 'saveCurrentInfo',
        componentId: 'saveCurrentInfo',
        hintText: false,
        menuLabel: 'Сохранить',
        undefinedItem: true
    },
    {
        actionTypeCallBack: 'closeModal',
        elementId: 'writeOut',
        componentId: 'writeOut',
        hintText: false,
        menuLabel: 'Подписать и отправить',
        undefinedItem: true
    }

]
}
