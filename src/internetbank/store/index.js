import Vue from 'vue'
import Vuex from 'vuex'
import { getField, updateField } from 'vuex-map-fields'
import generalReviewScenario from '@/internetbank/data/generalReviewScenario'
import billsScenario from '@/internetbank/data/billsScenario'
import gettingCurrencyScenario from '@/internetbank/data/gettingCurrencyScenario'
import createMassPaymentScenario from '@/internetbank/data/createMassPaymentScenario'
import createMassPaymentRubScenario from '@/internetbank/data/createMassPaymentRubScenario'
import lettersScenario from '@/internetbank/data/lettersScenario'
import productsAndServicesScenario from '@/internetbank/data/productsAndServicesScenario'
Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        currentScenarioType: null,
        isImport: false,
        showModal: false,
        showModal1: false,
        isActiveBtn: false,
        isActiveBtn1: true,
        isActiveBtn2: false,
        isActive: true,
        isActive2: false,
        isVisibleField: true,
        isActive1: true,
        activeTab1: true,
        activeTab2: false,
        isActive3: true,
        isActive4: true,
        isActive5: true,
        isActive6: true,
        isActive7: true,
        isActivetion: true,
        filledData: {
            fileImport: null,
            fileInput: '0171ca31-d585-17f4-bfad-5429f669830f',
            fileInput1: 'Импорт документов начат - 30.04.2020 11:26:35 \nИмпорт массового платежа завершен - 30.04.2020 11:26:45',
            fileInput2: 'Windows',
            fileInput3: 'Обработано',
            fileInput4: '3',
            fileInput5: '3',
            fileInput6: '3',
            fileInput7: '0',
            fileInput8: '0',
            fileInput9: '0',
            fileInput10: '186700,00',
            fileInput11: '1c_to_kl'
        },
        filledDataRub: {
            fileImport: null,
            fileInput: '0171ca31-d585-17f4-bfad-5429f669830f',
            fileInput1: 'Импорт документов начат - 30.04.2020 11:26:35 \nИмпорт документов завершен - 30.04.2020 11:26:45',
            fileInput2: 'Windows',
            fileInput3: 'Обработано',
            fileInput4: '1',
            fileInput5: '1',
            fileInput6: '1',
            fileInput7: '0',
            fileInput8: '0',
            fileInput9: '0',
            fileInput10: '1,11',
            fileInput11: '1c_to_kl'
        },
        filledData1: {
            fileInput: 'Иван Иванов',
            fileInput1: '123456',
            fileInput2: '04.06.2020',
            fileInput3: '840',
            fileInput4: '1000,00',
            fileInput5: 'USD'
        },
        checkmark: {
            checkmark1: false,
            checkmark2: false,
            checkmark3: false,
            checkmark4: false
        },
        checkMassMark: {
            checkMassMark1: false,
            checkMassMark2: false,
            checkMassMark3: false,
            checkMassMark5: false
        },
        scenarioList: {
            generalReview: generalReviewScenario,
            bills: billsScenario,
            gettingCurrency: gettingCurrencyScenario,
            createMassPayment: createMassPaymentScenario,
            createMassPaymentRub: createMassPaymentRubScenario,
            letters: lettersScenario,
            productsAndServices: productsAndServicesScenario
        }
    },
    getters: {
        getField,
        nameOfCurrentScenario (state) {
            return state.scenarioList[state.currentScenarioType].name
        },
        currentStep (state) {
            return state.scenarioList[state.currentScenarioType].currentStep
        },
        scenario (state) {
            return state.scenarioList[state.currentScenarioType].scenario
        },
        isFinishScenario (state) {
            return state.scenarioList[state.currentScenarioType].currentStep === state.scenarioList[state.currentScenarioType].scenario.length
        },
        availableScenarioTypes (state) {
            return Object.keys(state.scenarioList)
        },
        undefinedMenuItems (state) {
            return state.scenarioList[state.currentScenarioType].scenario.filter(item => {
                return item.hasOwnProperty('undefinedItem')
            })
        }
    },
    mutations: {
        updateField,
        restartScenario (state) {
           state.scenarioList[state.currentScenarioType].currentStep = 0
        },
        nextStep (state) {
            state.scenarioList[state.currentScenarioType].currentStep++
        },
        beforeStep (state) {
            state.scenarioList[state.currentScenarioType].currentStep--
        },
        setScenarioType (state, scenarioType) {
            state.currentScenarioType = scenarioType
        },
        setStep (state, step) {
            state.scenarioList[state.currentScenarioType].currentStep = step
        },
        modifiedScenarioData (state, data) {
            state.scenarioList[state.currentScenarioType].scenario.forEach(stateItem => {
                const label = stateItem.menuLabel ? stateItem.menuLabel.toLowerCase() : ''
                data.forEach(item => {
                    if (item.menuLabel.toLowerCase() === label) {
                        stateItem.elementId = item.elementId
                        stateItem.componentId = item.componentId
                        return false
                    }
                })
            })
        },
        setCheckBoxs (state) {
            for (const key in state.checkmark) {
                if (state.checkmark.hasOwnProperty(key)) {
                    state.checkmark[key] = true
                }
            }
        }
    },
    actions: {
        restartScenario ({ commit }) {
            commit('restartScenario')
        },
        nextStep ({ commit }) {
            commit('nextStep')
        },
        beforeStep ({ commit }) {
            commit('beforeStep')
        },
        setStep ({ commit }, step) {
            commit('setStep', step)
        },
        setScenarioType ({ commit }, scenarioType) {
            commit('setScenarioType', scenarioType)
        },
        modifiedScenarioData ({ commit }, data) {
            commit('modifiedScenarioData', data)
        },
        setCheckBoxs ({ commit }) {
            commit('setCheckBoxs')
        }
    },
    strict: process.env.NODE_ENV !== 'production'
})
