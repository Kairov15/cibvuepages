import axios from 'axios'
import { merge } from 'lodash'

export default class {
    constructor () {
        this.siteApiUrl = 'https://site-api.centrinvest.ru'
        this.requestUrl = process.env.NODE_ENV !== 'production' || window.location.host !== 'www.centrinvest.ru' ? 'https://showroom7.dev.cinet.ru/' : 'https://www.centrinvest.ru/'
    }

    get (url) {
        return axios.get(url)
    }

    post (url, data, config = {}) {
        let configMod = {}
        if (data instanceof FormData) {
            configMod.headers = {
                'Content-Type': 'multipart/form-data'
            }
        }
        configMod = merge(configMod, config)
        return axios.post(url, data, configMod)
    }
};
