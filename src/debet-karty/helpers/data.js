const filterTypes = [
    {
        text: 'Вознаграждение за покупки',
        name: 'RewardForPurchases',
        itemsArr: [],
        advantagesId: ['Рубли', 'Баллы']
    },
    {
        text: 'Другие преимущества',
        name: 'OtherAdvantages',
        itemsArr: [],
        advantagesId: ['Бесплатное обслуживание', 'Процент на остаток', 'Программа лояльности от платежной системы']
    }
]

export {
    filterTypes
}
