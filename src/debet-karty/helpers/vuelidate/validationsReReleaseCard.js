import { required, sameAs } from 'vuelidate/lib/validators'
import phone from '@/validators/phone'
import emailFullCheck from '@/validators/emailFullCheck'
import cardNumber from '@/validators/cardNumber'

export default {
    reReleaseCardModel: {
        reReleaseReasons: {
            required
        },
        fullName: {
            required
        },
        phone: {
            required,
            phone
        },
        email: {
            emailFullCheck
        },
        passportSeriesAndNumber: {
            required
        },
        cardNumber: {
            cardNumber
        },
        agreement: {
            required,
            sameAs: sameAs(() => true)
        }
    }
}
