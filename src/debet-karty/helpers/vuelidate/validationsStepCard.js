import { required, requiredIf, sameAs } from 'vuelidate/lib/validators'
import rusAndLatinLetters from '@/validators/rusAndLatinLetters'
import latinLetters from '@/validators/latinLetters'
import phone from '@/validators/phone'
import emailFullCheck from '@/validators/emailFullCheck'
import maxPreviousDay from '@/validators/maxPreviousDay'
import dateFormat from '@/validators/dateFormat'
import passportNumberAndSeries from '@/validators/passportNumberAndSeries'
import indexValidator from '@/validators/indexValidator'

export default function validationsStepCard () {
    const Step10 = {
        cardData: {
            cardClass: {
                required
            },
            cardCurrency: {
                required
            },
            cardTariff: {
                required
            }
        },
        contactData: {
            fullName: {
                required,
                rusAndLatinLetters
            },
            customTranslit: {
                required,
                latinLetters
            },
            phone: {
                required,
                phone
            },
            email: {
                required: requiredIf(() => {
                    return this.isHandballClubCard || this.isChaykaCard
                }),
                emailFullCheck
            },
            birthDate: {
                required,
                maxPreviousDay,
                dateFormat
            },
            citizenship: {
                required
            },
            agreement: {
                required,
                sameAs: sameAs(() => true)
            }
        }
    }
    const CustomDesign = {
        indivDesignData: {
            activeImage: {
                required: requiredIf(() => {
                    return this.isIndividualDesign
                })
            },
            activeFrontImage: {
                required: requiredIf(() => {
                    return this.isIndividualDesign
                })
            },
            cardMirPosition: {
                required: requiredIf(() => {
                    return this.isIndividualDesign && this.isMirPaymentSystem
                })
            },
            logoColor: {
                required: requiredIf(() => {
                    return this.isIndividualDesign
                })
            },
            logoPosition: {
                required: requiredIf(() => {
                    return this.isIndividualDesign
                })
            },
            previewHeight: {
                required: requiredIf(() => {
                    return this.isIndividualDesign
                })
            },
            previewLeftPadding: {
                required: requiredIf(() => {
                    return this.isIndividualDesign
                })
            },
            previewTopPadding: {
                required: requiredIf(() => {
                    return this.isIndividualDesign
                })
            },
            previewWidth: {
                required: requiredIf(() => {
                    return this.isIndividualDesign
                })
            }
        }
    }
    const Step20 = {
        passportData: {
            passportSeriesAndNumber: {
                required,
                passportNumberAndSeries: passportNumberAndSeries(this.isRusCitizenship)
            },
            issuedDate: {
                required,
                maxPreviousDay,
                dateFormat
            },
            issuedCode: {
                required: requiredIf(() => {
                    return this.isRusCitizenship
                })
            },
            issuedName: {
                required: requiredIf(() => {
                    return this.isRusCitizenship
                })
            },
            birthPlace: {
                required,
                rusAndLatinLetters
            }
        },
        addressData: {
            regAddress: {
                required
            },
            regZipIndex: {
                required,
                indexValidator: indexValidator(this.isRusCitizenship)
            },
            factAddress: {
                required: requiredIf(() => {
                    return !this.model.addressData.isFactAddress
                })
            },
            factZipIndex: {
                required: requiredIf(() => {
                    return !this.model.addressData.isFactAddress
                }),
                indexValidator: indexValidator(this.isRusCitizenship)
            }
        },
        additionalData: {
            taxResidences: {
                required: requiredIf(() => {
                    return !this.isRusCitizenship
                }),
                $each: {
                    taxResidence: {
                        required: requiredIf(() => {
                            return !this.isRusCitizenship
                        })
                    },
                    innOrTin: {
                        required: requiredIf(() => {
                            return !this.isRusCitizenship
                        })
                    }
                }
            }
        }
    }
    const Step30 = {
        receivingData: {
            method: {
                required
            },
            office: {
                required: requiredIf(() => {
                    return this.isOfficeReceiving
                })
            },
            officeCity: {
                required: requiredIf(() => {
                    return this.isOfficeReceiving
                })
            },
            officeRegion: {
                required: requiredIf(() => {
                    return this.isOfficeReceiving
                })
            },
            deliveryAddress: {
                required: requiredIf(() => {
                    return this.isDeliveryReceiving && this.isFanCard
                })
            }
        }
    }
    const validationGroup = [
        'Step10',
        'CustomDesign',
        'Step20',
        'Step30'
    ]
    return {
        Step10,
        CustomDesign,
        Step20,
        Step30,
        validationGroup
    }
}
