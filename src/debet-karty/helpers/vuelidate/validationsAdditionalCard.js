import { required, requiredIf, sameAs } from 'vuelidate/lib/validators'
import rusAndLatinLetters from '@/validators/rusAndLatinLetters'
import latinLetters from '@/validators/latinLetters'
import cardNumber from '@/validators/cardNumber'
import phone from '@/validators/phone'
import emailFullCheck from '@/validators/emailFullCheck'
import maxPreviousDay from '@/validators/maxPreviousDay'
import dateFormat from '@/validators/dateFormat'
import passportNumberAndSeries from '@/validators/passportNumberAndSeries'
import indexValidator from "@/validators/indexValidator";

export default function validationsAdditionalCard () {
    const Step10 = {
        cardClass: {
            required
        },
        cardRecipient: {
            required
        },
        cardHolderAdditionalFullName: {
            required: requiredIf(() => {
                return this.isForAThirdPersonRecipientType
            }),
            rusAndLatinLetters
        },
        fullName: {
            required,
            rusAndLatinLetters
        },
        customTranslit: {
            required,
            latinLetters
        },
        cardNumber: {
            required,
            cardNumber
        },
        phone: {
            required,
            phone
        },
        cardHolderAdditionalPhone: {
            required: requiredIf(() => {
                return this.isForAThirdPersonRecipientType
            }),
            phone
        },
        email: {
            emailFullCheck
        },
        birthDate: {
            required: requiredIf(() => {
                return this.isForAThirdPersonRecipientType
            }),
            maxPreviousDay,
            dateFormat
        },
        citizenship: {
            required: requiredIf(() => {
                return this.isForAThirdPersonRecipientType
            })
        },
        agreement: {
            required,
            sameAs: sameAs(() => true)
        }
    }
    let Step20 = {}
    if (this.isForAThirdPersonRecipientType) {
        Step20 = {
            passportSeriesAndNumber: {
                required,
                passportNumberAndSeries: passportNumberAndSeries(this.isRusCitizenship)
            },
            issuedCode: {
                required: requiredIf(() => {
                    return this.isRusCitizenship
                })
            },
            issuedDate: {
                required
            },
            issuedName: {
                required: requiredIf(() => {
                    return this.isRusCitizenship
                })
            },
            birthPlace: {
                required
            },
            factAddress: {
                required: requiredIf(() => {
                    return !this.isFactAddress
                })
            },
            factZipIndex: {
                required: requiredIf(() => {
                    return !this.isFactAddress
                }),
                indexValidator: indexValidator(this.isRusCitizenship)
            },
            regAddress: {
                required
            },
            regZipIndex: {
                required,
                indexValidator: indexValidator(this.isRusCitizenship)
            },
            taxResidences: {
                required: requiredIf(() => {
                    return !this.isRusCitizenship && this.isCitizenshipNotEmpty
                }),
                $each: {
                    taxResidence: {
                        required: requiredIf(() => {
                            return !this.isRusCitizenship && this.isCitizenshipNotEmpty
                        })
                    },
                    innOrTin: {
                        required: requiredIf(() => {
                            return !this.isRusCitizenship && this.isCitizenshipNotEmpty
                        })
                    }
                }
            }
        }
    }
    const Step30 = {
        method: {
            required
        },
        office: {
            required: requiredIf(() => {
                return this.isOfficeReceiving
            })
        },
        officeCity: {
            required: requiredIf(() => {
                return this.isOfficeReceiving
            })
        },
        officeRegion: {
            required: requiredIf(() => {
                return this.isOfficeReceiving
            })
        },
        deliveryAddress: {
            required: requiredIf(() => {
                return this.isDeliveryReceiving
            })
        }
    }
    const validationGroup = [
        'Step10',
        'Step20',
        'Step30'
    ]
    return {
        Step10,
        Step20,
        Step30,
        validationGroup
    }
}
