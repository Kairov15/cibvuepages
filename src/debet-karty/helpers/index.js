import { cardTypes, cardCurrencyes } from '@/debet-karty/model/requestModel'
import { merge } from 'lodash'
import Vue from 'vue'

function modifyPaymentSystems (paymentSystems) {
    return paymentSystems.map(paymentSystem => {
        const cardType = cardTypes.find(type => {
            return type.name === paymentSystem.slug
        })
        return {
            img: cardType ? cardType.img : null,
            ...paymentSystem
        }
    })
}

function modifyCurrencies (currencies) {
    return currencies.map(currency => {
        const currencyType = cardCurrencyes.find(type => {
            return type.name === currency.slug
        })
        return {
            img: currencyType ? currencyType.img : null,
            ...currency
        }
    })
}

async function pause (ms) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve()
        }, ms)
    })
}

function errorToast (payload) {
    let options = {
        message: 'Ошибка',
        type: 'error',
        duration: 3000,
        dismissible: true,
        position: 'top'
    }
    options = merge(options, payload)
    Vue.$toast.open(options)
}

function successToast (payload) {
    let options = {
        message: 'Успех',
        type: 'success',
        duration: 3000,
        dismissible: true,
        position: 'top'
    }
    options = merge(options, payload)
    Vue.$toast.open(options)
}

function errorHandler (error) {
    if (error.response) {
        errorToast({
            message: 'Ошибка загрузки данных'
        })
    } else if (error.request) {
        errorToast({
            message: 'Ошибка сервера'
        })
    } else {
        errorToast({
            message: error.message
        })
    }
    console.log(error.config)
    return Promise.reject(error)
}

const disabledDates = {
    maxDate: new Date(),
    min18Years: min18Years(),
    min14Years: min14Years()
}

function min18Years () {
    const date = new Date()
    const fullYear = date.getFullYear()
    date.setFullYear(fullYear - 18)
    return date
}

function min14Years () {
    const date = new Date()
    const fullYear = date.getFullYear()
    date.setFullYear(fullYear - 14)
    return date
}

export {
    modifyPaymentSystems,
    modifyCurrencies,
    pause,
    errorHandler,
    errorToast,
    successToast,
    disabledDates
}
