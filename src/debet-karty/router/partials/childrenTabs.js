import childrenNewCard from '@/debet-karty/router/partials/childrenNewCard'
import childrenAdditionalCard from '@/debet-karty/router/partials/childrenAdditionalCard'

const childrenTabs = (pagePrefix, newCardRequestTitle = '') => {
    return [
        {
            path: 'new-card',
            name: `${pagePrefix}-newCard`,
            component: () => import('@/debet-karty/views/cardsForms/newCard'),
            meta: {
                bcLinkText: 'Заказать новую'
            },
            props: {
                stepPrefix: pagePrefix,
                newCardRequestTitle
            },
            children: childrenNewCard(pagePrefix)
        },
        {
            path: 'additional-card',
            name: `${pagePrefix}-additionalCard`,
            component: () => import('@/debet-karty/views/cardsForms/additionalCard'),
            meta: {
                bcLinkText: 'Заказать дополнительную'
            },
            props: {
                pagePrefix: pagePrefix
            },
            children: childrenAdditionalCard(pagePrefix)
        },
        {
            path: 're-release-card',
            name: `${pagePrefix}-reReleaseCard`,
            component: () => import('@/debet-karty/views/cardsForms/reReleaseCard'),
            meta: {
                bcLinkText: 'Перевыпустить'
            }
        }
    ]
}

export default childrenTabs
