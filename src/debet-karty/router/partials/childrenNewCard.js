const childrenNewCard = (stepPrefix) => {
    return [
        {
            path: 'step1',
            name: `${stepPrefix}-Step10`,
            component: () => import('@/debet-karty/views/request/Step10'),
            meta: {
                bcLinkText: 'Шаг 1',
                logStep: 1
            }
        },
        {
            path: 'step2',
            name: `${stepPrefix}-Step20`,
            component: () => import('@/debet-karty/views/request/Step20'),
            meta: {
                bcLinkText: 'Шаг 2',
                logStep: 2
            }
        },
        {
            path: 'custom-design',
            name: `${stepPrefix}-CustomDesign`,
            component: () => import('@/debet-karty/views/request/CustomDesign'),
            meta: {
                bcLinkText: 'Выбор дизайна',
                logStep: 3
            }
        },
        {
            path: 'step3',
            name: `${stepPrefix}-Step30`,
            component: () => import('@/debet-karty/views/request/Step30'),
            meta: {
                bcLinkText: 'Шаг 3',
                logStep: 4
            }
        }
    ]
}

export default childrenNewCard
