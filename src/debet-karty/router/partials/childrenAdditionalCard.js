const childrenAdditionalCard = (pagePrefix) => {
    return [
        {
            path: 'step10',
            name: `${pagePrefix}-additionalCard-Step10`,
            component: () => import('@/debet-karty/views/cardsForms/additionalCard/Step10'),
            meta: {
                bcLinkText: 'Шаг 1',
                logStep: 1
            }
        },
        {
            path: 'step20',
            name: `${pagePrefix}-additionalCard-Step20`,
            component: () => import('@/debet-karty/views/cardsForms/additionalCard/Step20'),
            meta: {
                bcLinkText: 'Шаг 2',
                logStep: 2
            }
        },
        {
            path: 'step30',
            name: `${pagePrefix}-additionalCard-Step30`,
            component: () => import('@/debet-karty/views/cardsForms/additionalCard/Step30'),
            meta: {
                bcLinkText: 'Шаг 3',
                logStep: 3
            }
        }
    ]
}

export default childrenAdditionalCard
