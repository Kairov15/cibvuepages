import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'
import innerPage from '../views/innerPage.vue'
import childrenTabs from "./partials/childrenTabs";
const appPath = process.env.NODE_ENV === 'production' ? '/ru/fiz/bankovskie-karty/new-products/' : '/debet-karty'
Vue.use(VueRouter)

const homePageNewCardRequestTitle = 'Заказать Универсальную карту'
const innerPageNewCardRequestTitle = 'Заявка на выпуск карты'

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
            bcLinkText: 'Дебетовые карты'
        },
        beforeEnter: async (to, from, next) => {
            store.commit('card/setHomePageDataReceived', false)
            const homePageDataReceived = store.state.card.homePageDataReceived
            let error = false
            if (!homePageDataReceived) {
                store.commit('card/togglePreloader', true)
                try {
                    await Promise.all([
                        store.dispatch('card/getCardsList'),
                        store.dispatch('card/getFilters'),
                        store.dispatch('card/getAdvantagesTypes'),
                        store.dispatch('customDesign/getIndividualDesigns')
                    ])
                } catch (e) {
                    error = true
                }
                store.commit('card/setHomePageDataReceived', true)
                store.commit('card/togglePreloader', false)
            }
            if (!error) {
                await next()
            }
        },
        props: {
            pagePrefix: 'homePage'
        },
        children: childrenTabs('homePage', homePageNewCardRequestTitle)
    },
    {
        path: '/:slug',
        name: 'innerPage',
        meta: {
            getter: 'onePage',
            bcLinkText: (name) => name
        },
        component: innerPage,
        beforeEnter: async (to, from, next) => {
            store.commit('card/togglePreloader', true)
            const slug = to.params.slug
            let error = false
            try {
                await Promise.all([
                    store.dispatch('card/getCardDetail', slug),
                    store.dispatch('card/getAdvantagesTypes'),
                    store.dispatch('card/getCardsList'),
                    store.dispatch('customDesign/getIndividualDesigns')
                ])
            } catch (e) {
                error = true
            }
            store.commit('card/togglePreloader', false)
            if (!error) {
                await next()
            }
        },
        props: {
            pagePrefix: 'innerPage'
        },
        children: childrenTabs('innerPage')
    },
    {
        path: '*',
        redirect: '/'
    }
]

const router = new VueRouter({
    mode: 'history',
    base: appPath,
    routes
})

export default router
