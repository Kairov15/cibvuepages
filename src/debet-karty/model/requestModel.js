const model = {
    id: null,
    instanceId: null,
    target: 0,
    isCustomNameOnCard: false,
    cardData: {
        cardTariff: null,
        cardCurrency: null,
        cardClass: null,
        cardDesign: null,
        isCardDesignCustom: null
    },
    myIndivDesignData: {
        currentTab: null,
        currentTabLabel: 'Категории',
        currentImgId: null,
        designTab: 'classic'
    },
    indivDesignData: {
        activeImage: null,
        activeFrontImage: null,
        cardMirPosition: null,
        logoColor: null,
        logoPosition: null,
        previewHeight: null,
        previewLeftPadding: null,
        previewTopPadding: null,
        previewWidth: null
    },
    contactData: {
        birthDate: null,
        birthPlace: null,
        customTranslit: null,
        email: null,
        fullName: null,
        phone: null,
        citizenship: null,
        agreement: null
    },
    passportData: {
        issuedCode: null,
        issuedDate: null,
        issuedName: null,
        passportSeriesAndNumber: null
    },
    addressData: {
        factAddress: null,
        factZipIndex: null,
        isFactAddress: false,
        regAddress: null,
        regZipIndex: null
    },
    additionalData: {
        taxResidences: [
            {
                innOrTin: null,
                taxResidence: null
            }
        ]
    },
    receivingData: {
        method: null,
        office: null,
        officeCity: null,
        officeRegion: null,
        deliveryAddress: null
    }
}

const reReleaseCardModel = {
    target: 3,
    reReleaseReasons: null,
    fullName: null,
    phone: null,
    email: null,
    passportSeriesAndNumber: null,
    cardNumber: null,
    agreement: null
}

const additionalCardModel = {
    target: 1,
    isCustomNameOnCard: false,
    mainInfo: {
        cardClass: null,
        cardRecipient: null,
        fullName: null,
        customTranslit: null,
        cardHolderAdditionalFullName: null,
        cardNumber: null,
        phone: null,
        cardHolderAdditionalPhone: null,
        email: null,
        birthDate: null,
        citizenship: null,
        agreement: false
    },
    mainDocument: {
        passportSeriesAndNumber: null,
        issuedCode: null,
        issuedDate: null,
        issuedName: null,
        birthPlace: null,
        factAddress: null,
        factZipIndex: null,
        isFactAddress: false,
        regAddress: null,
        regZipIndex: null,
        taxResidences: [
            {
                innOrTin: null,
                taxResidence: null
            }
        ]
    },
    receivingData: {
        method: null,
        office: null,
        officeCity: null,
        officeRegion: null,
        deliveryAddress: null
    }
}

const cardTypes = [
    {
        name: 'visa',
        img: require('../assets/img/visa.svg')
    },
    {
        name: 'mscard',
        img: require('../assets/img/masterCard.svg')
    },
    {
        name: 'mir',
        img: require('../assets/img/mir.svg')
    }
]

const cardRecipientTypes = [
    {
        name: 'forYourself',
        label: 'Для себя'
    },
    {
        name: 'forAThirdPerson',
        label: 'Для третьего лица'
    }
]

const cardCurrencyes = [
    {
        name: 'rub',
        label: 'Рубль',
        img: require('../assets/img/mobile-bank-section/ruble.svg')
    },
    {
        name: 'usd',
        label: 'Доллар',
        img: require('../assets/img/mobile-bank-section/dollar.svg')
    },
    {
        name: 'euro',
        label: 'Евро',
        img: require('../assets/img/mobile-bank-section/euro.svg')
    }
]

const reReleaseReasons = [
    {
        name: 'cardExpiryDate',
        label: 'Срок действия карты'
    },
    {
        name: 'breaking',
        label: 'Поломка'
    },
    {
        name: 'loss',
        label: 'Утеря'
    },
    {
        name: 'suspectedFraud',
        label: 'Подозрение на мошенничество'
    }
]

const citizenshipOptions = [
    {
        text: 'Гражданство РФ',
        value: 'rus'
    },
    {
        text: 'Другое',
        value: 'other'
    }
]

const cardReceivingMethods = [
    {
        text: 'В офисе',
        value: 'office'
    },
    {
        text: 'Доставка',
        value: 'delivery'
    }
]

const stepInfo = [
    {
        name: 'Step10'
    },
    {
        name: 'Step20'
    },
    {
        name: 'CustomDesign'
    },
    {
        name: 'Step30'
    }
]

const additionalCardStepInfo = [
    {
        name: 'Step10'
    },
    {
        name: 'Step20'
    },
    {
        name: 'Step30'
    }
]

const logoColors = [
    {
        text: 'Зеленый',
        value: 'green'
    },
    {
        text: 'Белый',
        value: 'white'
    }
]

const logoPositions = [
    {
        text: 'Слева',
        value: 'left'
    },
    {
        text: 'Справа',
        value: 'right'
    }
]

const cardMirPositions = [
    {
        text: 'Сверху',
        value: 'up'
    },
    {
        text: 'Снизу',
        value: 'down'
    }
]

export {
    model,
    cardTypes,
    cardCurrencyes,
    citizenshipOptions,
    stepInfo,
    cardReceivingMethods,
    logoColors,
    logoPositions,
    cardMirPositions,
    reReleaseReasons,
    reReleaseCardModel,
    additionalCardModel,
    cardRecipientTypes,
    additionalCardStepInfo
}
