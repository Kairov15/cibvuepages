export default {
    // url: 'https://showroom20.dev.cinet.ru',
    url: 'https://site-api.centrinvest.ru',
    requestUrl: process.env.NODE_ENV !== 'production' || window.location.host !== 'www.centrinvest.ru' ? 'https://showroom7.dev.cinet.ru/' : 'https://www.centrinvest.ru/',
    mainCardUrl () {
        return `${this.requestUrl}cabinet/card-request/api/send`
    },
    reReleaseUrl () {
        return `${this.requestUrl}cabinet/card-request/api/send/rerelease`
    },
    mainCardFirstStepUrl () {
        return `${this.requestUrl}cabinet/card-request/api/send/step1`
    },
    fixInstanceIdUrl () {
        return `${this.requestUrl}cabinet/card-request/api/load`
    }
}
