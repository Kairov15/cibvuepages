import { getField, updateField } from 'vuex-map-fields'
import { reReleaseCardModel, additionalCardModel } from '@/debet-karty/model/requestModel'
import ApiClient from '@/debet-karty/apiClient'
import { cloneDeep } from 'lodash'
export default {
    namespaced: true,
    state: {
        reReleaseCardModel: cloneDeep(reReleaseCardModel),
        additionalCardModel: cloneDeep(additionalCardModel)
    },
    getters: {
        getField,
        reReleaseCard (state) {
            return {
                reReleaseReasons: state.reReleaseCardModel.reReleaseReasons,
                fullName: state.reReleaseCardModel.fullName,
                phone: state.reReleaseCardModel.phone,
                email: state.reReleaseCardModel.email,
                passportSeriesAndNumber: state.reReleaseCardModel.passportSeriesAndNumber,
                cardNumber: state.reReleaseCardModel.cardNumber,
                agreement: state.reReleaseCardModel.agreement
            }
        },
        additionalCard (state) {
            return {
                Step10: {
                    cardClass: state.additionalCardModel.mainInfo.cardClass,
                    cardRecipient: state.additionalCardModel.mainInfo.cardRecipient,
                    fullName: state.additionalCardModel.mainInfo.fullName,
                    cardHolderAdditionalFullName: state.additionalCardModel.mainInfo.cardHolderAdditionalFullName,
                    customTranslit: state.additionalCardModel.mainInfo.customTranslit,
                    cardNumber: state.additionalCardModel.mainInfo.cardNumber,
                    phone: state.additionalCardModel.mainInfo.phone,
                    cardHolderAdditionalPhone: state.additionalCardModel.mainInfo.cardHolderAdditionalPhone,
                    email: state.additionalCardModel.mainInfo.email,
                    birthDate: state.additionalCardModel.mainInfo.birthDate,
                    citizenship: state.additionalCardModel.mainInfo.citizenship,
                    agreement: state.additionalCardModel.mainInfo.agreement
                },
                Step20: {
                    passportSeriesAndNumber: state.additionalCardModel.mainDocument.passportSeriesAndNumber,
                    issuedCode: state.additionalCardModel.mainDocument.issuedCode,
                    issuedDate: state.additionalCardModel.mainDocument.issuedDate,
                    issuedName: state.additionalCardModel.mainDocument.issuedName,
                    birthPlace: state.additionalCardModel.mainDocument.birthPlace,
                    factAddress: state.additionalCardModel.mainDocument.factAddress,
                    factZipIndex: state.additionalCardModel.mainDocument.factZipIndex,
                    isFactAddress: state.additionalCardModel.mainDocument.isFactAddress,
                    regAddress: state.additionalCardModel.mainDocument.regAddress,
                    regZipIndex: state.additionalCardModel.mainDocument.regZipIndex,
                    taxResidences: state.additionalCardModel.mainDocument.taxResidences
                },
                Step30: {
                    method: state.additionalCardModel.receivingData.method,
                    office: state.additionalCardModel.receivingData.office,
                    officeCity: state.additionalCardModel.receivingData.officeCity,
                    officeRegion: state.additionalCardModel.receivingData.officeRegion,
                    deliveryAddress: state.additionalCardModel.receivingData.deliveryAddress
                }
            }
        },
        isForYourselfRecipientType (state) {
            return state.additionalCardModel.mainInfo.cardRecipient === 'Для себя'
        },
        isForAThirdPersonRecipientType (state) {
            return state.additionalCardModel.mainInfo.cardRecipient === 'Для третьего лица'
        },
        isOfficeReceiving (state) {
            return state.additionalCardModel.receivingData.method === 'office'
        },
        isDeliveryReceiving (state) {
            return state.additionalCardModel.receivingData.method === 'delivery'
        },
        isRusCitizenship (state) {
            return state.additionalCardModel.mainInfo.citizenship === 'rus'
        },
        isFactAddress (state) {
            return state.additionalCardModel.mainDocument.isFactAddress
        },
        isCitizenshipNotEmpty (state) {
            return state.additionalCardModel.mainInfo.citizenship
        }
    },
    mutations: {
        updateField
    },
    actions: {
        async sendReReleaseRequest ({ commit }, data) {
            try {
                const response = await ApiClient.sendReReleaseRequest(data)
                return response
            } catch (e) {
                return Promise.reject(e)
            }
        },
        async sendAdditionalCardRequest ({ commit }, data) {
            try {
                const response = await ApiClient.sendAdditionalCardRequest(data)
                return response
            } catch (e) {
                return Promise.reject(e)
            }
        }
    }
}
