import { getField, updateField } from 'vuex-map-fields'
import { cardMirPositions, logoColors, logoPositions } from '@/debet-karty/model/requestModel'
import axios from 'axios'
import requiest from '@/debet-karty/model/requiest'
import { errorHandler } from '../helpers'

export default {
    namespaced: true,
    state: {
        individualDesigns: [],
        host: 'https://www.centrinvest.ru/',
        logoColors: logoColors,
        logoPositions: logoPositions,
        cardMirPositions: cardMirPositions
    },
    getters: {
        getField,
        isIndividualDesign (state, getters, rootState) {
            return rootState.cardRequest.model.cardData.cardDesign === 'individual'
        },
        isMirPaymentSystem (state, getters, rootState) {
            let result = false
            let findObj = null
            if (rootState.card.onePage.dataReceived) {
                findObj = rootState.card.onePage.data.paymentSystems.find(paymentSystem => {
                    return paymentSystem.title === rootState.cardRequest.model.cardData.cardClass
                })
                result = findObj ? findObj.slug === 'mir' : false
            }
            return result
        },
        isMsCardPaymentSystem (state, getters, rootState) {
            let result = false
            let findObj = null
            if (rootState.card.onePage.dataReceived) {
                findObj = rootState.card.onePage.data.paymentSystems.find(paymentSystem => {
                    return paymentSystem.title === rootState.cardRequest.model.cardData.cardClass
                })
                result = findObj ? findObj.slug === 'mscard' : false
            }
            return result
        },
        isVisaPaymentSystem (state, getters, rootState) {
            let result = false
            let findObj = null
            if (rootState.card.onePage.dataReceived) {
                findObj = rootState.card.onePage.data.paymentSystems.find(paymentSystem => {
                    return paymentSystem.title === rootState.cardRequest.model.cardData.cardClass
                })
                result = findObj ? findObj.slug === 'visa' : false
            }
            return result
        }
    },
    mutations: {
        updateField,
        setIndividualDesigns (state, individualDesigns) {
            state.individualDesigns = individualDesigns.map(design => {
                return {
                    ...design,
                    categoryImg: design.pictures[0].thumbnail
                }
            })
        }
    },
    actions: {
        async getIndividualDesigns ({ commit, dispatch }) {
            try {
                const data = await axios.get(`${requiest.url}/api/v1/personal/cards/gallery`)
                const individualDesigns = data.data.payload.items
                commit('setIndividualDesigns', individualDesigns)
            } catch (e) {
                await errorHandler(e)
            }
        }
    }
}
