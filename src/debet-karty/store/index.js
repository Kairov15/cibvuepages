import Vue from 'vue'
import Vuex from 'vuex'
import card from './card'
import cardRequest from './cardRequest'
import customDesign from './customDesign'
import cardsForms from '@/debet-karty/store/cardsForms'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        card,
        cardRequest,
        customDesign,
        cardsForms
    }
})
