import { getField, updateField } from 'vuex-map-fields'
import router from '../router'
import cyrillicToTranslit from 'cyrillic-to-translit-js'
import ApiClient from '@/debet-karty/apiClient'
import { modifyCurrencies, modifyPaymentSystems, pause } from '@/debet-karty/helpers'

export default {
    namespaced: true,
    state: {
        cardType: 0,
        advantagesCheckbox: [],
        onePage: {
            data: {},
            dataReceived: false
        },
        fanCards: ['gk-rostov-card', 'karta-bolelshchika-futbolnogo-kluba-chaika'],
        supremeCards: ['mir-supreme', 'mir-supreme-plus'],
        cardsList: [],
        showPreloader: false,
        filters: [],
        advantagesTypes: [],
        homePageDataReceived: false,
        isHomePageCardDataRecieved: false,
        filteredCardList: []
    },
    getters: {
        getField,
        onePage (state) {
            return state.onePage
        },
        cardDesigins (state, getters, rootState) {
            if (!state.onePage.data.designs) {
                return []
            }
            return state.onePage.data.designs.filter(design => {
                return design.paymentSystem.title === rootState.cardRequest.model.cardData.cardClass
            })
        },
        isFanCard (state) {
            let result = false
            if (Object.prototype.hasOwnProperty.call(state.onePage.data, 'slug')) {
                result = state.fanCards.includes(state.onePage.data.slug)
            }
            return result
        },
        isSupremeCards (state) {
            let result = false
            if (Object.prototype.hasOwnProperty.call(state.onePage.data, 'slug')) {
                result = state.supremeCards.includes(state.onePage.data.slug)
            }
            return result
        },
        isChaykaCard (state) {
            return state.onePage.data.slug === 'karta-bolelshchika-futbolnogo-kluba-chaika'
        },
        isUniversalMirCard (state) {
            return state.onePage.data.slug === 'mir'
        },
        isUniversalCard (state) {
            return state.onePage.data.slug === 'univ'
        },
        isPensCard (state) {
            return state.onePage.data.slug === 'pens'
        },
        isHandballClubCard (state) {
            return state.onePage.data.slug === 'gk-rostov-card'
        },
        isSelfemployedCard (state) {
            return state.onePage.data.slug === 'selfemployed-card'
        },
        isNarodCard (state) {
            return state.onePage.data.slug === 'narod-mir'
        },
        isPremVisaCard (state) {
            return state.onePage.data.slug === 'premium'
        },
        isExclusiveVisaCard (state) {
            return state.onePage.data.slug === 'exclusive'
        },
        isServiceMirCard (state) {
            return state.onePage.data.slug === 'servisnaia-karta-mir'
        },
        isSupremeCard (state) {
            return state.onePage.data.slug === 'mir-supreme'
        },
        isSupremePlus (state) {
            return state.onePage.data.slug === 'mir-supreme-plus'
        },
        bgImgClass (state, getters) {
            return {
                'card-inner-mobile__background-img_chayka': getters.isChaykaCard,
                'card-inner-mobile__background-img_universalMir': getters.isUniversalMirCard,
                'card-inner-mobile__background-img_universal': getters.isUniversalCard,
                'card-inner-mobile__background-img_pens': getters.isPensCard,
                'card-inner-mobile__background-img_handballClub': getters.isHandballClubCard,
                'card-inner-mobile__background-img_selfemployed': getters.isSelfemployedCard,
                'card-inner-mobile__background-img_narod': getters.isNarodCard,
                'card-inner-mobile__background-img_premVisa': getters.isPremVisaCard,
                'card-inner-mobile__background-img_exclusiveVisa': getters.isExclusiveVisaCard,
                'card-inner-mobile__background-img_serviceMir': getters.isServiceMirCard,
                'card-inner-mobile__background-img_supremeCard': getters.isSupremeCard,
                'card-inner-mobile__background-img_supremePlus': getters.isSupremePlus
            }
        },
        isOneDesign (state, getters) {
            return getters.cardDesigins.length <= 1
        },
        isNotChooseDesign (state, getters, rootState, rootGetters) {
            const isMirPaymentSystem = rootGetters['customDesign/isMirPaymentSystem']
            const isVisaPaymentSystem = rootGetters['customDesign/isVisaPaymentSystem']
            const isFanCard = getters.isFanCard
            const isSelfemployedCard = getters.isSelfemployedCard
            const isExclusiveVisaCard = getters.isExclusiveVisaCard
            const isOneDesign = getters.isOneDesign
            return (!isMirPaymentSystem && !isVisaPaymentSystem && isOneDesign) || isFanCard || isSelfemployedCard || isExclusiveVisaCard
        }
    },
    mutations: {
        updateField,
        setTitle (state, title) {
            const translit = new cyrillicToTranslit()
            const result = translit.reverse(title).replace(/_/g, ' ')
            state.pageTitle = result
        },
        setCardsList (state, cardsList) {
            let cardListCopy = [...cardsList]
            cardListCopy.forEach(cardItem => {
                cardItem.paymentSystems = modifyPaymentSystems(cardItem.paymentSystems)
            })
            cardListCopy = cardListCopy.filter(cardItem => {
                return !state.supremeCards.includes(cardItem.slug)
            })
            state.cardsList = cardListCopy
        },
        togglePreloader (state, value) {
            state.showPreloader = value
        },
        setHomePageDataReceived (state, value) {
            state.homePageDataReceived = value
        },
        resetPageData (state) {
            state.onePage.data = {}
            state.onePage.dataReceived = false
        },
        setInnerCardPage (state, innerCard) {
            innerCard.paymentSystems = modifyPaymentSystems(innerCard.paymentSystems)
            innerCard.currencies = modifyCurrencies(innerCard.currencies)
            state.onePage.data = innerCard
            state.onePage.dataReceived = true
        },
        setFilters (state, filters) {
            state.filters = filters
        },
        setAdvantagesTypes (state, advantagesTypes) {
            state.advantagesTypes = advantagesTypes
        },
        filterCardList (state) {
            state.filteredCardList = state.cardsList.filter(card => {
                const filterMap = {
                    cardType: true,
                    advantages: true
                }
                if (state.cardType === 0) {
                    filterMap.cardType = true
                } else {
                    const findType = card.filters.find(filter => {
                        return filter.id === state.cardType
                    })
                    filterMap.cardType = !!findType
                }
                if (!state.advantagesCheckbox.length) {
                    filterMap.advantages = true
                } else {
                    const allAdvantagesArr = card.advantages.map(advantage => {
                        return advantage.type
                    })
                    const allFounded = state.advantagesCheckbox.every(adv => allAdvantagesArr.includes(adv))
                    filterMap.advantages = allFounded
                }

                return Object.values(filterMap).every(result => result)
            })
        }
    },
    actions: {
        async getCardsList ({ commit }) {
            const cardsList = await ApiClient.getCardList()
            commit('setCardsList', cardsList)
        },
        async getFilters ({ commit }) {
            const filters = await ApiClient.getFilters()
            commit('setFilters', filters)
        },
        async getAdvantagesTypes ({ commit }) {
            const advantagesTypes = await ApiClient.getAdvantagesTypes()
            commit('setAdvantagesTypes', advantagesTypes)
        },
        async getCardDetail ({ commit }, slug) {
            try {
                const innerCard = await ApiClient.getCardDetail(slug)
                commit('resetPageData')
                commit('setInnerCardPage', innerCard)
            } catch (e) {
                await router.push({ name: 'Home' })
            }
        },
        async filterCardList ({ commit }) {
            commit('togglePreloader', true)
            await pause(300)
            await new Promise(resolve => {
                commit('filterCardList')
                resolve()
            })
            commit('togglePreloader', false)
        }
    }
}
