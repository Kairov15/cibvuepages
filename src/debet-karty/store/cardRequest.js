import { getField, updateField } from 'vuex-map-fields'
import { model, cardTypes, cardCurrencyes, citizenshipOptions, stepInfo, cardReceivingMethods } from '@/debet-karty/model/requestModel'
import { BankBranchesApiClient, SERVICES_OPERU } from '@/services/BankBranchesApiClient'
import cloneDeep from 'lodash/cloneDeep'
import { errorHandler } from '../helpers'
import ApiClient from '@/debet-karty/apiClient'
export default {
    namespaced: true,
    state: {
        model: cloneDeep(model),
        cardTypes: cardTypes,
        cardCurrencyes: cardCurrencyes,
        citizenshipOptions: citizenshipOptions,
        stepInfo: stepInfo,
        cardReceivingMethods: cardReceivingMethods,
        currentStep: 0,
        regions: [],
        branches: [],
        placemarks: {},
        isValidForPotential: false,
        imagePreview: null
    },
    getters: {
        getField,
        localities (state) {
            if (!state.model.receivingData.officeRegion) {
                return []
            }

            const region = state.regions.find((element, index) => {
                return element.value === state.model.receivingData.officeRegion
            })
            return region ? region.cities : []
        },
        branches (state) {
            if (!state.model.receivingData.officeCity) {
                return []
            }

            return state.branches.filter((branch) => {
                return branch.city_id === state.model.receivingData.officeCity
            })
        },
        isOfficeReceiving (state) {
            return state.model.receivingData.method === 'office'
        },
        isDeliveryReceiving (state) {
            return state.model.receivingData.method === 'delivery'
        },
        isIndividualDesign (state) {
            return state.model.cardData.cardDesign === 'individual'
        },
        isRusCitizenship (state) {
            return state.model.contactData.citizenship === 'rus'
        },
        Step10 (state) {
            return {
                cardData: {
                    cardClass: state.model.cardData.cardClass,
                    cardCurrency: state.model.cardData.cardCurrency,
                    cardTariff: state.model.cardData.cardTariff
                },
                contactData: {
                    fullName: state.model.contactData.fullName,
                    customTranslit: state.model.contactData.customTranslit,
                    phone: state.model.contactData.phone,
                    email: state.model.contactData.email,
                    birthDate: state.model.contactData.birthDate,
                    citizenship: state.model.contactData.citizenship,
                    agreement: state.model.contactData.agreement
                }
            }
        },
        Step20 (state) {
            return {
                passportData: {
                    passportSeriesAndNumber: state.model.passportData.passportSeriesAndNumber,
                    issuedDate: state.model.passportData.issuedDate,
                    issuedCode: state.model.passportData.issuedCode,
                    issuedName: state.model.passportData.issuedName,
                    birthPlace: state.model.contactData.birthPlace
                },
                addressData: {
                    regAddress: state.model.addressData.regAddress,
                    regZipIndex: state.model.addressData.regZipIndex,
                    factAddress: state.model.addressData.factAddress,
                    factZipIndex: state.model.addressData.factZipIndex,
                    isFactAddress: state.model.addressData.isFactAddress
                },
                additionalData: {
                    taxResidences: state.model.additionalData.taxResidences
                }
            }
        },
        CustomDesign (state) {
            return {
                indivDesignData: {
                    activeImage: state.model.indivDesignData.activeImage,
                    activeFrontImage: state.model.indivDesignData.activeFrontImage,
                    cardMirPosition: state.model.indivDesignData.cardMirPosition,
                    logoColor: state.model.indivDesignData.logoColor,
                    logoPosition: state.model.indivDesignData.logoPosition,
                    previewHeight: state.model.indivDesignData.previewHeight,
                    previewLeftPadding: state.model.indivDesignData.previewLeftPadding,
                    previewTopPadding: state.model.indivDesignData.previewTopPadding,
                    previewWidth: state.model.indivDesignData.previewWidth
                }
            }
        },
        Step30 (state) {
            return {
                receivingData: {
                    method: state.model.receivingData.method,
                    office: state.model.receivingData.office,
                    officeCity: state.model.receivingData.officeCity,
                    officeRegion: state.model.receivingData.officeRegion,
                    deliveryAddress: state.model.receivingData.deliveryAddress
                }
            }
        }
    },
    mutations: {
        updateField,
        setBranchesData (state, data) {
            state.branches = data.branches
            state.regions = data.regions
            state.placemarks = data.placemarks
        },
        setInstanceId (state, instanceId) {
            state.model.instanceId = instanceId
        }
    },
    actions: {
        async sendMainCardRequest ({ commit }, data) {
            const response = await ApiClient.sendMainCardRequest(data)
            return response
        },
        async fixInstanceId ({ state, commit }) {
            if (!state.model.instanceId) {
                const response = await ApiClient.fixInstanceId()
                const instanceId = response.data.instanceId
                commit('setInstanceId', instanceId)
            }
        },
        async loadBranches ({ commit }) {
            commit('card/togglePreloader', true, { root: true })
            try {
                const regions = await BankBranchesApiClient.getFullRegions(SERVICES_OPERU)
                const branches = await BankBranchesApiClient.getFullOffices(SERVICES_OPERU)
                const placemarks = await BankBranchesApiClient.getMapPoints(SERVICES_OPERU)
                const data = { regions, branches, placemarks }
                commit('setBranchesData', data)
            } catch (error) {
                await errorHandler(error)
            } finally {
                commit('card/togglePreloader', false, { root: true })
            }
        },
        async sendPotentialRequest ({ state }) {
            if (state.isValidForPotential) {
                await ApiClient.sendPotentialRequest(state.model)
            }
        },
        async sendStepStat ({ state }, payload) {
            await ApiClient.sendStepStatistic(payload)
        }
    }
}
