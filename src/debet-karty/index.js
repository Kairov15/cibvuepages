import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Notifications from 'vue-notification'
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-sugar.css'
import { errorHandler } from './helpers'
import VueMeta from 'vue-meta'
Vue.use(VueMeta)
Vue.use(Notifications)
Vue.use(VueToast)

Vue.prototype.$isProduction = process.env.NODE_ENV === 'production'
Vue.prototype.$errorHandler = errorHandler

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
