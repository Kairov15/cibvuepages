import BaseApiClient from '@/baseApiClient'
import { errorHandler } from '@/debet-karty/helpers'

class ApiClient extends BaseApiClient {
    urls = {
        cardsList: `${this.siteApiUrl}/api/v1/personal/cards/products`,
        filters: `${this.siteApiUrl}/api/v1/personal/cards/filters`,
        advantagesTypes: `${this.siteApiUrl}/api/v1/personal/cards/filters`,
        cardDetail: (slug) => `${this.siteApiUrl}/api/v1/personal/cards/products/${slug}`,
        mainRequest: `${this.requestUrl}cabinet/card-request/api/send`,
        fixInstanceId: `${this.requestUrl}cabinet/card-request/api/load`,
        potentialRequest: `${this.requestUrl}cabinet/card-request/api/send/step1`,
        stepStatistic: `${this.requestUrl}cabinet/card-request/api/record-stat`,
        reReleaseRequest: `${this.requestUrl}cabinet/card-request/api/send/rerelease`,
        additionalCardRequest: `${this.requestUrl}cabinet/card-request/api/send/**********`
    }

    async getCardList () {
        try {
            const response = await this.get(this.urls.cardsList)
            const cardsList = response.data.payload.items
            return cardsList
        } catch (e) {
            await errorHandler(e)
            return Promise.reject(e)
        }
    }

    async getFilters () {
        try {
            const response = await this.get(this.urls.filters)
            const filters = response.data.payload.items
            return filters
        } catch (e) {
            await errorHandler(e)
            return Promise.reject(e)
        }
    }

    async getAdvantagesTypes () {
        try {
            const response = await this.get(this.urls.advantagesTypes)
            const advantagesTypes = response.data.payload.items
            return advantagesTypes
        } catch (e) {
            await errorHandler(e)
            return Promise.reject(e)
        }
    }

    async getCardDetail (slug) {
        try {
            const response = await this.get(this.urls.cardDetail(slug))
            const cardDetail = response.data.payload
            return cardDetail
        } catch (e) {
            await errorHandler(e)
            return Promise.reject(e)
        }
    }

    async sendMainCardRequest (data, config = {}) {
        try {
            const response = await this.post(this.urls.mainRequest, data, config)
            return response
        } catch (e) {
            await errorHandler(e)
            return Promise.reject(e)
        }
    }

    async fixInstanceId () {
        try {
            const response = await this.get(this.urls.fixInstanceId)
            return response
        } catch (e) {
            await errorHandler(e)
            return Promise.reject(e)
        }
    }

    async sendPotentialRequest (data, config = {}) {
        try {
            const response = await this.post(this.urls.potentialRequest, data, config)
            return response
        } catch (e) {
            return Promise.reject(e)
        }
    }

    async sendStepStatistic (data, config = {}) {
        try {
            const response = await this.post(this.urls.stepStatistic, data, config)
            return response
        } catch (e) {
            return Promise.reject(e)
        }
    }

    async sendReReleaseRequest (data, config = {}) {
        try {
            const response = await this.post(this.urls.reReleaseRequest, data, config)
            return response
        } catch (e) {
            await errorHandler(e)
            return Promise.reject(e)
        }
    }

    async sendAdditionalCardRequest (data, config = {}) {
        try {
            const response = await this.post(this.urls.additionalCardRequest, data, config)
            return response
        } catch (e) {
            await errorHandler(e)
            return Promise.reject(e)
        }
    }
}

export default new ApiClient()
