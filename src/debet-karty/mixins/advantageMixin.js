const advantageMixin = {
    props: {
        advantages: {
            type: Array,
            default: () => {
                return []
            }
        },
        advantageId: {
            type: String,
            required: true
        },
        contentCenter: {
            type: Boolean
        }
    },
    methods: {
        findAdvantageByType (type) {
            return this.advantages.find(adv => {
                return adv.type === type
            })
        }
    }
}

export default advantageMixin
