import cibInput from 'centrinvest-web-components/src/appComponents/cibInput'
import { validationMixin } from 'vuelidate'
const requiestsMixin = {
    props: {
        $v: {
            type: Object,
            default: () => ({})
        }
    },
    components: {
        cibInput
    },
    mixins: [validationMixin]
}

export default requiestsMixin
