import cibInput from 'centrinvest-web-components/src/appComponents/cibInput'
import cibLink from 'centrinvest-web-components/src/appComponents/link'
import cibButton from 'centrinvest-web-components/src/appComponents/Button'
import { mapFields } from 'vuex-map-fields'
import { disabledDates } from '@/debet-karty/helpers'

const stepMixin = {
    components: {
        cibInput,
        cibLink,
        cibButton
    },
    props: {
        $v: {
            type: Object,
            default () {
                return {}
            }
        }
    },
    data: () => ({
        disabledDates
    }),
    computed: {
        ...mapFields('cardRequest', [
            'model'
        ])
    }
}

export default stepMixin
