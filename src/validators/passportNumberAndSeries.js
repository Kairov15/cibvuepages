import { helpers } from 'vuelidate/lib/validators'

export default (param) =>
    helpers.withParams(
        { type: 'passportNumberAndSeries', value: param },
        (value) => {
            let result = true
            if (param) {
                result = !helpers.req(value) || /^([0-9]{4}\s[0-9]{6})$/.test(value)
            }
            return result
        }
    )
