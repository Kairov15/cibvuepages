import * as helpers from 'vuelidate/lib/validators/common'
export default helpers.withParams({ type: 'emailFullCheck' }, (value) => {
    const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return !helpers.req(value) || pattern.test(value)
})
