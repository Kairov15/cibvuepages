import * as helpers from 'vuelidate/lib/validators/common'
export default helpers.regex('cardNumber', /^([0-9]{4}\sXXXX\sXXXX\s[0-9]{4})$/)
