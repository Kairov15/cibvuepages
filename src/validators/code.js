import * as helpers from 'vuelidate/lib/validators/common'
export default helpers.regex('code', /^([0-9]{6})$/)
