import * as helpers from 'vuelidate/lib/validators/common'
export default helpers.withParams({ type: 'rusAndLatinLetters' }, (value) => {
    return !helpers.req(value) || /^([а-яёa-z\-\s.,;]+)$/ig.test(value)
})
