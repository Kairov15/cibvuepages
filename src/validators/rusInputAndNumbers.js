import * as helpers from 'vuelidate/lib/validators/common'
export default helpers.withParams({ type: 'rusInputAndNumbers' }, (value) => {
    return !helpers.req(value) || /^([А-Яа-яёЁ\-,.();:'"0-9?!#№/\s]+)$/g.test(value)
})
