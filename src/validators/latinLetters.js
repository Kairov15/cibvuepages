import * as helpers from 'vuelidate/lib/validators/common'
export default helpers.withParams({ type: 'latinLetters' }, (value) => {
    return !helpers.req(value) || /^([a-z\s]+)$/ig.test(value)
})
