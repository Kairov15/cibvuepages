import * as helpers from 'vuelidate/lib/validators/common'
export default helpers.regex('postCode', /^([0-9]{6})$/)
