import * as helpers from 'vuelidate/lib/validators/common'
export default helpers.regex('passportDivisionCode', /^([0-9]{3}-[0-9]{3})$/)
