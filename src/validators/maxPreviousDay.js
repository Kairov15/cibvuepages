import * as helpers from 'vuelidate/lib/validators/common'
import moment from 'moment'
export default helpers.withParams({ type: 'maxPreviousDay' }, (value) => {
    if (!value) {
        return true
    }
    const enteredDate = moment(value, 'DD.MM.YYYY')
    if (!enteredDate) {
        return true
    }
    return enteredDate < new Date()
})
