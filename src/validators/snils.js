import { helpers } from 'vuelidate/lib/validators'
export default helpers.regex('snils', /^[0-9]{3}-[0-9]{3}-[0-9]{3}-[0-9]{2}$/)
