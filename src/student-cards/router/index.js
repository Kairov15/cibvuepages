import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
import { pause } from '@/debet-karty/helpers'
const appPath = process.env.NODE_ENV === 'production' ? '/ru/student-cards' : '/student-cards'
Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: () => import('../views/Home'),
        beforeEnter: (to, from, next) => {
            store.commit('toggleModal', true)
            next()
        }
    },
    {
        path: '/:slug',
        name: 'cardPage',
        component: () => import('../views/cardPage'),
        beforeEnter: async (to, from, next) => {
            store.commit('togglePreloader', true)
            try {
                if (!store.getters.isIssetOffices) {
                    await store.dispatch('getOffices')
                }
                await pause(500)
                await next()
                store.commit('toggleModal', false)
            } catch (e) {
            } finally {
                store.commit('togglePreloader', false)
            }
        }
    }
]

const router = new VueRouter({
    mode: 'history',
    base: appPath,
    routes,
    store
})

export default router
