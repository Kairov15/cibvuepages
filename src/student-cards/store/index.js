import Vue from 'vue'
import Vuex from 'vuex'
import {
    getField,
    updateField
} from 'vuex-map-fields'
import axios from 'axios'
import { request } from '@/student-cards/data/model'
import { errorHandler } from '@/debet-karty/helpers'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        showModal: false,
        showMobileMenu: false,
        showPreloader: false,
        officesList: []
    },
    getters: {
        getField,
        isIssetOffices (state) {
            return state.officesList.length
        }
    },
    mutations: {
        updateField,
        togglePreloader (state, payload) {
            state.showPreloader = payload
        },
        toggleModal (state, payload) {
            state.showModal = payload
        },
        setOffices (state, offices) {
            const officesMod = offices.map(item => ({
                value: `${item.city_title}, ${item.text}`,
                id: item.id
            }))
            state.officesList = officesMod
        }
    },
    actions: {
        async getOffices ({ commit }) {
            try {
                const offices = (await axios.get(request.officesUrl())).data
                commit('setOffices', offices)
            } catch (e) {
                await errorHandler(e)
            }
        }
    },
    modules: {}
})
