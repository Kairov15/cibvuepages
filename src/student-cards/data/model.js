const debug = process.env.NODE_ENV !== 'production' || window.location.host !== 'www.centrinvest.ru'
const formData = {
    reason: null,
    fio: null,
    phone: null,
    birthday: null,
    nationality: null,
    email: null,
    office: null,
    agreement: false,
    pageUrl: null
}

const advantagesGroup1 = [
    {
        value: '3%',
        text: 'на&nbsp;книги и&nbsp;игры'
    },
    {
        value: '5%',
        text: 'на всё меню в <a href="https://bebopburgers.ru/delivery/" class="student-cards-advantages__info-link" target="_blank" rel="noopener noreferrer"> BeBop Burgers </a>'
    },
    {
        value: '20%',
        text: 'в клубе <a href="https://meta4pro.ru/" class="student-cards-advantages__info-link" target="_blank" rel="noopener noreferrer">Meta4Pro Bootcamp</a>'
    },
    {
        value: '1%',
        text: 'за любые покупки'
    }
]
const advantagesGroup2 = [
    {
        value: '3%',
        text: 'на книги и игры'
    },
    {
        value: '1%',
        text: 'за любые покупки'
    }
]

const cardsData = [
    {
        slug: 'sfedu',
        label: 'ЮФУ',
        img: require('@/student-cards/assets/images/cardsImg/sfedu.png'),
        advantages: advantagesGroup1
    },
    {
        slug: 'mgtu',
        label: 'МГТУ',
        img: require('@/student-cards/assets/images/cardsImg/mgtu__card.png'),
        advantages: advantagesGroup2
    },
    {
        slug: 'donstu',
        label: 'ДГТУ',
        img: require('@/student-cards/assets/images/cardsImg/donstu.png'),
        advantages: advantagesGroup1
    },
    {
        slug: 'ranepa',
        label: 'РАНХиГС',
        img: require('@/student-cards/assets/images/cardsImg/ranepa.png'),
        advantages: advantagesGroup1
    },
    {
        slug: 'rsue',
        label: 'РГЭУ (РИНХ)',
        img: require('@/student-cards/assets/images/cardsImg/rsue.png'),
        advantages: advantagesGroup1
    },
    {
        slug: 'iwtsedov',
        label: 'ИВТ им. Г.Я.',
        img: require('@/student-cards/assets/images/cardsImg/ivt.png'),
        advantages: advantagesGroup1
    },
    {
        slug: 'kubstu',
        label: 'КубГТУ',
        img: require('@/student-cards/assets/images/cardsImg/kubstu.png'),
        advantages: advantagesGroup2
    },
    {
        slug: 'rgups',
        label: 'РГУПС',
        img: require('@/student-cards/assets/images/cardsImg/rgups.png'),
        advantages: advantagesGroup1
    },
    {
        slug: 'rsk',
        label: 'РСК',
        img: require('@/student-cards/assets/images/cardsImg/rsk.png'),
        advantages: advantagesGroup1
    },
    {
        slug: 'npi',
        label: 'HПИ',
        img: require('@/student-cards/assets/images/cardsImg/npi.png'),
        advantages: advantagesGroup1
    }
]

const reasons = [
    'Поломка',
    'Утеря',
    'Подозрение на мошеничество'
]

const nationalities = [
    'Российская Федерация',
    'Другое'
]

const request = {
    url: debug ? 'https://cib3.dev.cinet.ru/ru/include/emailCardsSend' : '/ru/include/emailCardsSend',
    host: debug ? 'https://showroom7.dev.cinet.ru/' : '/',
    officesUrl (params = '') {
        return `${this.host}cabinet/ajax/yandexmapcoordinate2/?call=getFullOffices${params}`
    }
}

export { formData, cardsData, request, reasons, nationalities }
