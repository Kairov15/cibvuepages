import { required, sameAs } from 'vuelidate/lib/validators'
import ruDashSpace from '@/validators/ruDashSpace'
import phone from '@/validators/phone'
import maxPreviousDay from '@/validators/maxPreviousDay'
import emailFullCheck from '@/validators/emailFullCheck'
export default function validations () {
    return {
        formData: {
            reason: {
                required
            },
            fio: {
                required,
                ruDashSpace
            },
            phone: {
                required,
                phone
            },
            birthday: {
                required,
                maxPreviousDay
            },
            nationality: {
                required
            },
            email: {
                required,
                emailFullCheck
            },
            office: {
                required
            },
            agreement: {
                required,
                sameAs: sameAs(() => true)
            },
            pageUrl: {
                required
            }
        }
    }
}
