const path = require('path')
const isProduction = process.env.NODE_ENV === 'production'
const fileExtention = isProduction ? 'php' : 'html'

module.exports = {
    publicPath: isProduction
    ? '/ru/services/cibVuePages/dist'
    : '/',
    outputDir: './dist',
    filenameHashing: true,
    lintOnSave: false,
    productionSourceMap: !isProduction,
    css: {
        sourceMap: !isProduction,
        requireModuleExtension: true
    },
    pages: {
        index: {
            entry: 'src/main.js',
            template: 'public/index.html',
            filename: 'index.html',
            title: 'Index Page',
            chunks: ['chunk-vendors', 'chunk-common', 'index']
          },
        'kreditovaniye-biznesa': {
            entry: 'src/kreditovaniye-biznesa/index.js',
            template: `public/index.${fileExtention}`,
            filename: `kreditovaniye-biznesa/index.${fileExtention}`,
            title: 'Biz Page',
            chunks: ['chunk-vendors', 'chunk-common', 'kreditovaniye-biznesa']
        },
        'kreditnyye-karty': {
            entry: 'src/kreditnyye-karty/index.js',
            template: `public/index.${fileExtention}`,
            filename: `kreditnyye-karty/index.${fileExtention}`,
            title: 'Cards Page',
            chunks: ['chunk-vendors', 'chunk-common', 'kreditnyye-karty']
        },
        'debet-karty': {
            entry: 'src/debet-karty/index.js',
            template: `public/index.${fileExtention}`,
            filename: `debet-karty/index.${fileExtention}`,
            title: 'Подобрать и заказать дебетовую карту онлайн > Ростовская область | ПАО коммерческий банк Центр-инвест',
            chunks: ['chunk-vendors', 'chunk-common', 'debet-karty']
        },
        internetbank: {
            entry: 'src/internetbank/index.js',
            template: isProduction ? 'public/indexWithoutBitrix.php' : `public/index.${fileExtention}`,
            filename: `internetbank/index.${fileExtention}`,
            title: 'Internetbank Page',
            chunks: ['chunk-vendors', 'chunk-common', 'internetbank']
        },
        calendar: {
            entry: 'src/calendar/index.js',
            template: `public/index.${fileExtention}`,
            filename: `calendar/index.${fileExtention}`,
            title: 'Calendar',
            chunks: ['chunk-vendors', 'chunk-common', 'calendar']
        },
        'potreb-kredit': {
            entry: 'src/potreb-kredit/index.js',
            template: isProduction ? 'public/newIndexWithoutBitrix.php' : `public/index.${fileExtention}`,
            filename: `potreb-kredit/index.${fileExtention}`,
            title: 'potreb-kredit',
            chunks: ['chunk-vendors', 'chunk-common', 'potreb-kredit']
        },
        'prodaja-monet': {
            entry: 'src/prodaja-monet/index.js',
            template: isProduction ? 'public/coinsPage.php' : `public/index.${fileExtention}`,
            filename: `prodaja-monet/index.${fileExtention}`,
            title: 'prodaja-monet',
            chunks: ['chunk-vendors', 'chunk-common', 'prodaja-monet']
        },
        equiring: {
            entry: 'src/equiring/index.js',
            template: `public/index.${fileExtention}`,
            filename: `equiring/index.${fileExtention}`,
            title: 'equiring',
            chunks: ['chunk-vendors', 'chunk-common', 'equiring']
        },
        'child-card': {
            entry: 'src/child-card/index.js',
            template: isProduction ? 'public/indexWithoutBitrix.php' : `public/index.${fileExtention}`,
            filename: `child-card/index.${fileExtention}`,
            title: 'child-card',
            chunks: ['chunk-vendors', 'chunk-common', 'child-card']
        },
        'student-cards': {
            entry: 'src/student-cards/index.js',
            template: isProduction ? 'public/indexWithoutBitrix.php' : `public/index.${fileExtention}`,
            filename: `student-cards/index.${fileExtention}`,
            title: 'student-cards',
            chunks: ['chunk-vendors', 'chunk-common', 'student-cards']
        },
    },
    devServer: {
        historyApiFallback: {
            rewrites: [
                { from: /^\/debet-karty\/?.*/, to: path.posix.join('/', 'debet-karty/index.html') },
                { from: /^\/kreditnyye-karty\/?.*/, to: path.posix.join('/', 'kreditnyye-karty/index.html') },
                { from: /^\/kreditovaniye-biznesa\/?.*/, to: path.posix.join('/', 'kreditovaniye-biznesa/index.html') },
                { from: /^\/internetbank\/?.*/, to: path.posix.join('/', 'internetbank/index.html') },
                { from: /^\/calendar\/?.*/, to: path.posix.join('/', 'calendar/index.html') },
                { from: /^\/potreb-kredit\/?.*/, to: path.posix.join('/', 'potreb-kredit/index.html') },
                { from: /^\/prodaja-monet\/?.*/, to: path.posix.join('/', 'prodaja-monet/index.html') },
                { from: /^\/equiring\/?.*/, to: path.posix.join('/', 'equiring/index.html') },
                { from: /^\/child-card\/?.*/, to: path.posix.join('/', 'child-card/index.html') },
                { from: /^\/student-cards\/?.*/, to: path.posix.join('/', 'student-cards/index.html') },
            ]
        }
    },
    transpileDependencies: ['centrinvest-web-components', 'image-to-base64'],
    chainWebpack: config => {
        config.module
            .rule('vue')
            .use('vue-loader')
            .loader('vue-loader')
            .tap(options => {
                options.compilerOptions.whitespace = 'preserve'
                return options
            })

        config.module
            .rule('images')
            .use('url-loader')
            .loader('url-loader')
            .tap(options => Object.assign(options, {
                whitespace: 'preserve',
                limit: 10000
            }))

        const svgRule = config.module.rule('svg')
        svgRule.uses.clear()
        svgRule
            .use('url-loader')
            .loader('url-loader')
            .tap(options => {
                return {
                    fallback: {
                        loader: 'file-loader',
                        options: {
                            name: 'img/[name].[hash:8].[ext]'
                        }
                    },
                    limit: 10000
                }
            })

        config.module
            .rule('scss')
            .oneOf('vue')
            .use('resolve-url-loader')
            .loader('resolve-url-loader').options({
                keepQuery: true,
                sourceMap: true
            })
            .before('sass-loader')

        config.module
            .rule('scss')
            .oneOf('vue')
            .use('sass-loader')
            .loader('sass-loader')
            .tap(options => ({
                ...options,
                sourceMap: true
            }))
    }
}
